﻿using Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Threading.Tasks;


namespace TestDataReaderUtility
{
    public class TestDataReader
    {
        //Read data from Excel file
        public DataTable ReadDataFromExcelFile(string fileName, string  filePath, string sheetName)
        {
            //Open file and returns as Stream
            FileStream stream = File.Open(filePath+fileName, FileMode.Open,FileAccess.Read);
        
            //Create OpenXMLReader via ExcelReaderFActory
            IExcelDataReader excelRedaer = ExcelReaderFactory.CreateOpenXmlReader(stream);//Read xlsx file

            //Set first row as the column name
            excelRedaer.IsFirstRowAsColumnNames = true;

            //Return as Dataset
            DataSet dataSet = excelRedaer.AsDataSet();

            //Get all the Tables
            DataTableCollection dataTableCollections = dataSet.Tables;

            //Store it in Datatable
            DataTable dataTable = dataTableCollections[sheetName];

            return dataTable;
        }
    }
    
}
