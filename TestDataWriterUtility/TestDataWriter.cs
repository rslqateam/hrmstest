﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Excel = Microsoft.Office.Interop.Excel;

namespace TestDataWriterUtility
{
    public class TestDataWriter
    {
        //Data Table can be write to a Worksheet 
        public void UpdateDataToExcelWorksheet(DataTable dataTable, string fileName, string filePath, int worksheetIndex)
        {
            //Create an Excel application instance
            Excel.Application excelApp = new Excel.Application();

            //Create an Excel workbook instance and open it from the predefined location
            Excel.Workbook excelWorkBook = excelApp.Workbooks.Open(filePath + fileName);
            Excel.Worksheet excelWorkSheet = excelWorkBook.Worksheets[worksheetIndex];

            try
            {
                if (excelWorkSheet.Name == dataTable.TableName)
                {
                    for (int row = 0; row < dataTable.Rows.Count; row++)
                    {
                        for (int col = 0; col < dataTable.Columns.Count; col++)
                        {
                            excelWorkSheet.Cells[row + 2, col + 1] = dataTable.Rows[row].ItemArray[col].ToString();
                        }
                    }
                }
                
            }
            catch
            {
                excelWorkBook.Save();
                excelWorkBook.Close();
                excelApp.Quit();
            }
            excelWorkBook.Save();
            excelWorkBook.Close();
            excelApp.Quit();
        }//End Worksheer

        //Particular column will be updated
        public void UpdateDataToExcelWorksheetColumn(DataTable dataTable, string fileName, string filePath, int worksheetIndex, int columnIndex)
        {
            //Create an Excel application instance
            Excel.Application excelApp = new Excel.Application();

            //Create an Excel workbook instance and open it from the predefined location
            Excel.Workbook excelWorkBook = excelApp.Workbooks.Open(filePath + fileName);
            Excel.Worksheet excelWorkSheet = excelWorkBook.Worksheets[worksheetIndex];

            try
            {
                if (excelWorkSheet.Name == dataTable.TableName)
                {
                    for (int row = 0; row < dataTable.Rows.Count; row++)
                    {
                        excelWorkSheet.Cells[row + 2, columnIndex] = dataTable.Rows[row].ItemArray[columnIndex-1].ToString();
                        
                    }
                }

            }
            catch
            {
                excelWorkBook.Save();
                excelWorkBook.Close();
                excelApp.Quit();
            }
            excelWorkBook.Save();
            excelWorkBook.Close();
            excelApp.Quit();
        }


    }
}
