﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using HRMSUIAutomation.Utilities;
using HRMSUIAutomation.Page;

using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace HRMSUIAutomation.Test
{
    /// <summary>
    /// Summary description for LogInTest
    /// </summary>
    [TestClass]
    
    public class LogInTest
    {
        private IWebDriver driver;
        LogInPage logInPage;
        DashboardPage dashboardPage;
        
        [TestInitialize]
        public void TestInitialize()
        {

            driver = TestBrowser.InitializeDriver();

            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(30));
            driver.Navigate().GoToUrl(TestURL.URL);
            driver.Manage().Window.Maximize();

            logInPage = new LogInPage(driver);
            PageFactory.InitElements(driver, logInPage);
            dashboardPage = new DashboardPage(driver);
            PageFactory.InitElements(driver, dashboardPage);
        }


        //Test Case 001 : Verify that user can log in using valid username and password
        //Author : Kana
        [Description("Verify that user can log in using valid username and password")]
        [TestCategory("LogIN")]
        [Owner("Nadim")]
        [Priority(1)]
        [TestMethod]
        public void LogInWithValidUserNamePassword()
        {

            //Log in with username and password
            logInPage.ValidLogIn("admin", "123");
           
            //verify dashboard
            Assert.AreEqual("My Dashboard", dashboardPage.DashboardTitleText());
        }
    }
}
