﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using HRMSUIAutomation.Utilities;
using HRMSUIAutomation.Page;

using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;

namespace HRMSUIAutomation.Test
{
    [TestClass]
    public class CreateEmployeeRequisitionUS1
    {
        private IWebDriver driver;
        LogInPage logInPage;
        DashboardPage dashboardPage;
        EmployeeInformationPage employeeInformationPage;
        CreateEmployeeRequisitionPageUS1 createEmployeeRequisitionPageUS1;


        [TestInitialize]
        public void TestInitialize()
        {
            driver = TestBrowser.InitializeDriver();

            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(30));
            driver.Navigate().GoToUrl(TestURL.URL);
            driver.Manage().Window.Maximize();

            logInPage = new LogInPage(driver);
            PageFactory.InitElements(driver, logInPage);

            dashboardPage = new DashboardPage(driver);
            PageFactory.InitElements(driver, dashboardPage);

            employeeInformationPage = new EmployeeInformationPage(driver);
            PageFactory.InitElements(driver, employeeInformationPage);

            createEmployeeRequisitionPageUS1 = new CreateEmployeeRequisitionPageUS1(driver);
            PageFactory.InitElements(driver, createEmployeeRequisitionPageUS1);


        }

        //Create Employee Requisition User Story 1 
        //Author : Kana

        [TestMethod]
        public void CreateEmployeeRequisition()
        {
            logInPage.ValidLogIn("hr", "123");

            ////Temp code
            //IAlert alert = driver.SwitchTo().Alert();
            //alert.Accept();

            //driver.SwitchTo().DefaultContent();

            //Navigate to DashboradPage
            dashboardPage.NavigateToEmployeeRequisitionForm();

            //Select Designation for Employee Requisition Application
            createEmployeeRequisitionPageUS1.EmployeeRequisitionApplicationForDesignation("Senior Software QA Engineer");


            //Enter Required Employee for Employee Requisition Application
            createEmployeeRequisitionPageUS1.EmployeeRequisitionApplicationForRequiredEmployee("1");
            //Click Add button
           createEmployeeRequisitionPageUS1.AddButtonClickForEmployeeRequisitionApplication();
            //Click Employee Requisition Application to Expand All
           createEmployeeRequisitionPageUS1.EmployeeRequisitionApplicationToExpand("Senior Software QA Engineer");

            //Enter value to related field in the Employee Requisition Application
           createEmployeeRequisitionPageUS1.EmployeeRequisitionApplicationForInputField("Structure Data Systems Ltd. (SDSL)", "Development", "Software Development", "Code", "Team Lead", "Full Time", "Honor Degree", "5", "Not Required", "55000", "As per company rule", "Dhaka", "01/08/2016", "Male", "30", "Not Required", "RSL-3");

            //click Save button
           createEmployeeRequisitionPageUS1.SaveButtonClickForEmployeeRequisitionApplication();

            //Click Yes, I am button 
           createEmployeeRequisitionPageUS1.YesButtonClickForEmployeeRequisitionApplication();

            //Enter approval System
           //createEmployeeRequisitionPageUS1.SubmitEmployeeRequisitionForApproval( "Nabil Ud Daula-EMP 38908 (38908)", "please consider");
        }
    }

}