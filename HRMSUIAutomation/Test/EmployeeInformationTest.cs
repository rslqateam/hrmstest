﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using HRMSUIAutomation.Utilities;
using HRMSUIAutomation.Page;

using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;


namespace HRMSUIAutomation.Test
{
    [TestClass]
    public class EmployeeInformationTest
    {
        private IWebDriver driver;
        LogInPage logInPage;
        DashboardPage dashboardPage;
        EmployeeInformationPage employeeInformationPage;

        [TestInitialize]
        public void TestInitialize()
        {
            driver = TestBrowser.InitializeDriver();

            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(30));
            driver.Navigate().GoToUrl(TestURL.URL);
            driver.Manage().Window.Maximize();

            logInPage = new LogInPage(driver);
            PageFactory.InitElements(driver, logInPage);

            dashboardPage = new DashboardPage(driver);
            PageFactory.InitElements(driver, dashboardPage);

            employeeInformationPage = new EmployeeInformationPage(driver);
            PageFactory.InitElements(driver, employeeInformationPage);
            
            //Log in to the application
            logInPage.ValidLogIn("hr", "123");

        }


        
         //TC/Personal/008	Verify that error message is displayed for invalid selection ID
         //Author : Susmita Kar
         [TestMethod]
         public void ErrorMessageForInvalidSelectionId()
         {
            
             //Navigate to DashboradPage
             dashboardPage.NavigateToEmpSetupForm();           
             
             //load invalid selected employeee
             employeeInformationPage.SelectedEmployeeSearch("12345");

             //Verify error message for invalid Selection Id
             Assert.AreEqual("No Information found under Selection Id: 12345", employeeInformationPage.ErrorMessageForInvalidSelectionIdText());

         }

        public bool isTextPresent()
         {
             if (employeeInformationPage.EmployeeNameTextBox.Text.Length > 0)
                 return true;
             else
                 return false;
         }



        //TC/Personal/007	Verify that employee information can be searched by Selection ID
         //Author : Susmita Kar
         [TestMethod]
         public void EmployeeSetupBySelectionId()
         {
             

             //Navigate to DashboradPage
             dashboardPage.NavigateToEmpSetupForm();

             ///load a selected employee information
             employeeInformationPage.SelectedEmployeeSearch("24");

             //Verify Personal Tab Input Field Loading For Selection Id
             //driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(30));
             Thread.Sleep(TimeSpan.FromSeconds(30));

             //WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
             //wait.Until<Boolean>((d)>={isTextPresent();});

             //employeeInformationPage.EmployeeNameTextBox.SendKeys("Tareq Rahman");
            

             Assert.AreEqual("Tareq Rahman", employeeInformationPage.ReturnEmployeeName());
             Assert.AreEqual("Mostafizur Rahman", employeeInformationPage.ReturnFatherName());
             Assert.AreEqual("Sarika a", employeeInformationPage.ReturnMotherName());


            //Click Contact Information
             //employeeInformationPage.ContactInfoClickForInput();


            //Verify Contact Info Tab Input Field Loading For Selection Id
             Assert.AreEqual("Dhaka", employeeInformationPage.ReturnPresentAddress());
             Assert.AreEqual("Comilla", employeeInformationPage.ReturnPermanentAddress());
             Assert.AreEqual("tare21@gmail.com", employeeInformationPage.ReturnEmailAddress());

            //Click Job Details
             //employeeInformationPage.JobDetailsClick();
             Assert.AreEqual("Navana Ltd", employeeInformationPage.Returncompany());
             Assert.AreEqual("Navana Garments", employeeInformationPage.ReturnBusinessUnit());
             Assert.AreEqual("Navana IT", employeeInformationPage.ReturnDepartment());
             //Assert.AreEqual("Comilla", employeeInformationPage.ReturnSection());
             Assert.AreEqual("Software QA Engineer Test", employeeInformationPage.ReturnDesignation());
             Assert.AreEqual("07/05/2015", employeeInformationPage.ReturnJoiningDate());

        }






         //TC/Personal/17   Verify that error message is displayed for invalid Naitonal ID
         //Author : Susmita Kar

         [TestMethod]
         public void ErrorMessageForInvalidNationalId()
         {



             //Navigate to DashboradPage
             dashboardPage.NavigateToEmpSetupForm();

             //Passing value of National Id
             employeeInformationPage.NationalIdInput("123-456");



         }


         //TC/Personal/20	Verify that error message is displayed for dulplicated "National Id"
         //Author : Susmita Kar

         [TestMethod]
         public void ErrorMessageForDuplicatedNationalId()
         {



             //Navigate to DashboradPage
             dashboardPage.NavigateToEmpSetupForm();

             //Image Uploading Greater Than 1MB
             employeeInformationPage.NationalIdInput("1234567");


             //Verify ErrorMessage for Duplicated National Id

             //Assert.AreEqual("    ", employeeInformationPage.ErrorMessageForEnteringDuplicatedNationalId());
         }





         //TC/Personal/24   Verify that error message is displayed for mandatory field "Date of Birth"
         //Author : Susmita Kar

         [TestMethod]
         public void ErrorMessageForDateOfBirth()
         {



             //Navigate to DashboradPage
             dashboardPage.NavigateToEmpSetupForm();

             //Input Mendatory Field of Employee Setup Form Without Date Of Birth
             employeeInformationPage.EmployeeSetupFormInputWithoutDateOfBirth("Mahid", "Islam", "Uttara,Dhaka", "Shalgaria,Pabna", "Navana Ltd", "Navana IT", "Navana SQA", "Software Quality Assurance Engineer", "Night-Shift", "01/08/2015");

             //Verify Error Message For Date Of Birth
             Assert.AreEqual("This field is required.", employeeInformationPage.ErrorMessageForMandatoryFieldDateOfBirth());



         }


         //TC/Personal/029	Verify that error message is displayed if uploaded image is greater than 1 MB
         //Author : Susmita Kar

         [TestMethod]
         public void ErrorMessageForUploadingImageGreaterThan1MB()
         {


             //Navigate to DashboradPage
             dashboardPage.NavigateToEmpSetupForm();

             //Image Uploading Greater Than 1MB
             employeeInformationPage.ImageUploadGreaterThan1MB("C:\\Users\\User\\Downloads\\Snake_River_(5mb).jpg");

             //Verify error message for uploading image greater than 1MB
             Assert.AreEqual("The file size cannot exceed 1MB", employeeInformationPage.ErrorMessageImageUpload());

         }
  

        
        //TC/Personal/30	Verify that error message is displayed if uploaded image format is other than JPG,PNG,JPEG
         //Author : Susmita Kar

         [TestMethod]
         public void ErrorMessageForUploadingImageOfInvalidFormate()
         {
            
             //Navigate to DashboradPage
             dashboardPage.NavigateToEmpSetupForm();

             //Uploading Image of Other Format Like Doc
             employeeInformationPage.ImageUploadOfInvalidFormate(@"C:\Users\User\Downloads\1964.doc");


             //Verify Personal Tab Input Field Loading For Selection Id

             Assert.AreEqual("You can upload only jpeg/jpg/JEPG/JPG extensions files.", employeeInformationPage.ErrorMessageForInvalidFormatImageUpload());
         }
        



         //TC/ContactInfo/001 Verify that error message is displayed for mandatory field "Present Address"
        //Author : Susmita Kar
        [TestMethod]
        public void ErrorMessagegForPresentAddress()
        {
           
            //Navigate to DashboradPage
            dashboardPage.NavigateToEmpSetupForm();

            //Input Mendatory Field of Employee Setup Form Without Present Address
            employeeInformationPage.EmployeeSetupFormInputWithoutPresentAddress("Mahid", "Islam", "24/10/2013", "Uttara,Dhaka", "Shalgaria,Pabna", "Navana Ltd", "Navana IT", "Navana SQA", "Software Quality Assurance Engineer", "Night-Shift", "01/08/2015");

            //Verify Error Message For Present Address
            Assert.AreEqual("This field is required.", employeeInformationPage.ErrorMessageForMendatoryFieldPresentAddress());
        }

        
        //TC/ContactInfo/004 Verify that error message is displayed for mandatory field "Permanant Address"
        //Author : Susmita Kar
        [TestMethod]
        public void ErrorMessagegForPermanentAddress()
        {

            //Navigate to DashboradPage
            dashboardPage.NavigateToEmpSetupForm();

            //Input Mendatory Field of Employee Setup Form Without Permanent Address
            employeeInformationPage.EmployeeSetupFormInputWithoutPermanentAddress("Mahid", "Islam", "24/10/2013", "Uttara,Dhaka", "Shalgaria,Pabna", "Navana Ltd", "Navana IT", "Navana SQA", "Software Quality Assurance Engineer", "Night-Shift", "01/08/2015");

            //Verify Error Message For Present Address
            Assert.AreEqual("This field is required.", employeeInformationPage.ErrorMessageForMendatoryFieldPermanentAddress());
        }


        //TC/ContactInfo/009 Verify that error message is displayed for invalid email address
        //Author : Susmita Kar
        [TestMethod]
        public void ErrorMessagegForInvalidEmail()
        {

            //Navigate to DashboradPage
            dashboardPage.NavigateToEmpSetupForm();

            //Input invalid E-Mail
            employeeInformationPage.InvalidEmail("shuvra43yahoo.com");
            
            //Verify Error Message For Invalid Email
            Assert.AreEqual("Please enter a valid email address. Example: abc@xyz.com", employeeInformationPage.ErrorMessageForEnteringInvalidEmail());
        }

       
        
        //TC/JobDetails/004 Verify that clicking on "Business Unit" is displayed list of Business Unit name according to the company
        //Author : Susmita Kar
        [TestMethod]
        public void DisplayListOfBusinessUnitAccordingToCompany()
        {

            //Navigate to DashboradPage
            dashboardPage.NavigateToEmpSetupForm();

            //Diplay list of Business Unit According To Company
            employeeInformationPage.ListOfBusinessUnit("Navana Ltd", "Navana Development");

            //Verify Error Message For Invalid Email
            //Assert.AreEqual("Navana Development", employeeInformationPage.ReturnBusinessUnit());
        }







        //TC/JobDetails/026 Verify that error message is displayed if date format (dd/MM/YYYY) is wrong 
        //Author : Susmita Kar
        [TestMethod]
        public void ErrorMessageForInvalidDateFormat()
        {

            //Navigate to DashboradPage
            dashboardPage.NavigateToEmpSetupForm();

            //Passing Invalid Date in the field DateOfBirth
            employeeInformationPage.DateFormat("2015/07/29");

            //Verify Error Message For Invalid Email
            Assert.AreEqual("undefined NaN", employeeInformationPage.ErrorMessageDateFormat());
        }




        //TC/Weekly Holiday/001  Verify that "Weekly Holiday" name is not duplicate 
        //Author : Susmita Kar

        [TestMethod]
        public void WeeklyHolidaySelectCanNotBeDuplicated()
        {

            //Navigate to DashboradPage
            dashboardPage.NavigateToEmpSetupForm();

            //For the Selection of Multiple Holiday
            employeeInformationPage.SelectMultipleHolidayForCheckingDuplicateValue("Saturday", "Sunday");

            //Verify the Weekly Holiday can not be duplicated
            Assert.AreEqual("Saturday", employeeInformationPage.ReturnWeeklyHolidaySaturday());
            Assert.AreEqual("Sunday", employeeInformationPage.ReturnWeeklyHolidaySunday());



        }



        //TC/Weekly Holiday/002  Verify that error message is displayed for mandatory field "weekly holiday"
        //Author : Susmita Kar

        [TestMethod]
        public void ErrorMessageForWeeklyHoliday()
        {

            //Navigate to DashboradPage
            dashboardPage.NavigateToEmpSetupForm();

            //Input Mendatory Field of Employee Setup Form Without WeeklyHoliday
            employeeInformationPage.EmployeeSetupFormInputWithoutWeeklyHoliday("Mahid", "Islam", "24/10/2013", "Uttara,Dhaka", "Shalgaria,Pabna", "Navana Ltd", "Navana IT", "Navana SQA", "Software Quality Assurance Engineer", "Night-Shift", "01/08/2015", "Friday");

            //Verify error message for mandatory field Weekly Holiday 
            Assert.AreEqual("This field is required.", employeeInformationPage.ErrorMessageForMendatoryFieldWeeklyHoliday());

        }




        //TC/Weekly Holiday/003  Verify that user can set multiple "holiday" on "Weekly Holiday" field
        //Author : Susmita Kar

        [TestMethod]
        public void MultipleWeeklyHolidaySelect()
        {

            //Navigate to DashboradPage
            dashboardPage.NavigateToEmpSetupForm();

            //For the Selection of Multiple Holiday
            employeeInformationPage.SelectMultipleHoliday("Saturday", "Sunday");

            //Verify the selected Weekly Holiday displayed in the UI
            Assert.AreEqual("Saturday", employeeInformationPage.ReturnWeeklyHolidaySaturday());
            Assert.AreEqual("Sunday", employeeInformationPage.ReturnWeeklyHolidaySunday());



        }



        //TC/Weekly Holiday/004  Verify that user can add new "holiday" by entering holiday name in the "Weekly Holiday" field
        //Author : Susmita Kar

        [TestMethod]
        public void AddHolidayByEnteringInWeeklyHoliday()
        {

            //Navigate to DashboradPage
            dashboardPage.NavigateToEmpSetupForm();

            //For the Selection of Multiple Holiday
            employeeInformationPage.SelectMultipleHoliday("Saturday", "Sunday");

            //Verify the selected Weekly Holiday displayed in the UI
            Assert.AreEqual("Saturday", employeeInformationPage.ReturnWeeklyHolidaySaturday());
            Assert.AreEqual("Sunday", employeeInformationPage.ReturnWeeklyHolidaySunday());



        }


        //TC/Weekly Holiday/05 Verify that user can remove holiday from "Weekly Holiday" field by clicking (x) sign besides holiday name
        //Author : Susmita Kar
        [TestMethod]
        public void RemoveHolidayClickingOnCrossSign()
        {

            //Navigate to DashboradPage
            dashboardPage.NavigateToEmpSetupForm();

            //Remove Holiday  clicking (x) sign besides holiday name
            employeeInformationPage.RemoveWeeklyHoliday("Friday", "Saturday", "Sunday");


        }



        //For Employee Information Search


        [Description("TC/SearchEmployee/001 Verify that user can search an employee by using single or multiple Employee IDs")]
        [TestCategory("SearchEmployeeById")]
        [Owner(" Susmita Kar")]
        [Priority(1)]
        [TestMethod]

        public void SearchEmployeeBySingleOrMultipleEmpoloyeeId()
        {

            //Navigate to DashboradPage
            dashboardPage.NavigateToEmpSetupForm();

            //Search Employee by Single or Multiple Employee Id
            employeeInformationPage.SearchEmployeeId("14");

           //Verify Employee Id in the grid
            Assert.AreEqual("EMP 14", employeeInformationPage.ReturnSearchEmployeeID());


        }


        [Description("TC/SearchEmployee/002 Verify that user can search an employee by using Employee Name")]
        [TestCategory("SearchEmployeeByName")]
        [Owner(" Susmita Kar")]
        [Priority(2)]
        [TestMethod]

        public void SearchEmployeeByName()
        {

            //Navigate to DashboradPage
            dashboardPage.NavigateToEmpSetupForm();

            //Search Employee by Name
            employeeInformationPage.SearchEmployeeName("Susmita Kar");

            //Verify Employee Name in the grid
            Assert.AreEqual("Susmita Kar", employeeInformationPage.ReturnSearchEmployeeName());


        }


        [Description("TC/SearchEmployee/005 Verify that user can search an employee by using Employee ID and Name")]
        [TestCategory("SearchEmployeeByIdAndName")]
        [Owner(" Susmita Kar")]
        [Priority(3)]
        [TestMethod]

        public void SearchEmployeeByIdAndName()
        {

            //Navigate to DashboradPage
            dashboardPage.NavigateToEmpSetupForm();

            //Search Employee by Id and Name
            employeeInformationPage.SearchEmployeeIdName("14","Susmita Kar");

             //Verify Employee Id in the grid
            Assert.AreEqual("EMP 14", employeeInformationPage.ReturnSearchEmployeeID());

            //Verify Employee Id and Name in the grid
            Assert.AreEqual("Susmita Kar", employeeInformationPage.ReturnSearchEmployeeName());


        }



        [Description("TC/SearchEmployee/005 Verify that clicking on > (Next) button will display employee information from Next page of the employe list")]
        [TestCategory("CheckNextButtonClick")]
        [Owner(" Susmita Kar")]
        [Priority(2)]
        [TestMethod]

        public void CheckNextButtonClick()
        {

            //Navigate to DashboradPage
            dashboardPage.NavigateToEmpSetupForm();

            //Search Employee Creation Date
            employeeInformationPage.SearchEmployeeCreationDate("01/06/2015", "03/08/2015");

            //Verify Button 2 Click
            Assert.AreEqual("2", employeeInformationPage.ReturnButton2Click());


        }


        [Description("TC/SearchEmployee/012 Verify that employee information can be edited")]
        [TestCategory("EditInformation")]
        [Owner(" Susmita Kar")]
        [Priority(1)]
        [TestMethod]

        public void EditEmployeeInformation()
        {

            //Navigate to DashboradPage
            dashboardPage.NavigateToEmpSetupForm();

            //Search Employee Creation Date
            //employeeInformationPage.SearchEmployeeId("14");

            //For Editing Employee Information
            employeeInformationPage.EditingEmployeeInformation("14", "Not Availablesss");
            //employeeInformationPage.CheckingEditingEmployeeInformation("14");


            //Verify the Editing Information
            Assert.AreEqual("Not Availablesss", employeeInformationPage.ReturnEditedInformation());


        }



        [Description("TC/SearchEmployee/007 Verify that selecting the  record number from record per page drop down list will display number of records at the employee list")]
        [TestCategory("DisplayEmployeeListAccordingToSelectedNumner")]
        [Owner(" Susmita Kar")]
        [Priority(2)]
        [TestMethod]

        public void DisplayEmployeeListAccordingToSelectedNumner()
        {

            //Navigate to DashboradPage
            dashboardPage.NavigateToEmpSetupForm();

            //For Displaying Employee List in the grid according to selected number
            employeeInformationPage.DisplayEmployeeList("01/06/2015", "03/08/2015", "15");
           


            //Verify number of Employee in the grid
            


        }

        [Description("TC/SearchEmployee/008 Verify that Employee list can be sorted by column name")]
        [TestCategory("EmployeeSorted")]
        [Owner(" Susmita Kar")]
        [Priority(2)]
        [TestMethod]

        public void EmployeeSortedByName()
        {

            //Navigate to DashboradPage
            dashboardPage.NavigateToEmpSetupForm();

            //For sorting employee by name
            employeeInformationPage.SortingEmployeeByName("01/08/2015", "03/08/2015");



            //Verify the sorted Employee in the grid
            Assert.AreEqual("Easir Arafat", employeeInformationPage.ReturnFirstEmployeeName());
            Assert.AreEqual("Mahid", employeeInformationPage.ReturnSecondEmployeeName());
            Assert.AreEqual("Mahid", employeeInformationPage.ReturnThirdEmployeeName());
        }



       








          



        //[TestCleanup]
        //public void TestCleanUp()
        //{
        //    driver.Close();
        //} 

    }
}
