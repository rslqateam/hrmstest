﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using HRMSUIAutomation.Utilities;
using HRMSUIAutomation.Page;

using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using HRMSUIAutomation.Test;

namespace HRMSUIAutomation.Test
{
    [TestClass]
    public class BusinessUnitTest
    {
        private IWebDriver driver;
        LogInPage logInPage;
        DashboardPage dashboardPage;
        BusinessUnitPage businessUnitPage;


        [TestInitialize]
        public void TestInitialize()
        {
            driver = TestBrowser.InitializeDriver();

            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(30));
            driver.Navigate().GoToUrl(TestURL.URL);
            driver.Manage().Window.Maximize();

            logInPage = new LogInPage(driver);
            PageFactory.InitElements(driver, logInPage);

            dashboardPage = new DashboardPage(driver);
            PageFactory.InitElements(driver, dashboardPage);

            businessUnitPage = new BusinessUnitPage(driver);
            PageFactory.InitElements(driver, businessUnitPage);
            // Log in 
            logInPage.ValidLogIn("hr", "123");
        }

        //TC/BusinessUnit/001    Verify that "Code" is auto generated from database after saving information
        //Author : TareQ
        [Description(" Verify that Code is auto generated from database after saving information")]
        [TestCategory("Group of Company")]
        [Owner("TareQ")]
        [Priority(1)]
        [TestMethod]

        public void BusinessUnitCodeIsAutoGeneratedAfterSave()
        {

            //Navigate to DashboradPage
            dashboardPage.NavigateToBusinessUnitPage();
            // send mandatory fields
            businessUnitPage.AllInputFieldsforBusinessUnitSetup("Brac IT Test", "DevelopmentTest1", "ব্রাক আইটি ", "Road 5, House 115, Block B Niketan Society, Gulshan, Dhaka-1212, Bangladesh", "রোড ৫, হাউস ১১৫, ব্লক ব নিকেতন সোসাইটি , গুলশান , ঢাকা ১২১২, বাংলাদেশ", "3692587", "01787688644", "6985742", "info@bracits.com", "www.bracits.com","This is a strong Business unit of brac it");
            //verify success message
            Assert.AreEqual("BusinessUnit information saved successfully.", businessUnitPage.CheckSuccessMessage());


        }


    }
}
