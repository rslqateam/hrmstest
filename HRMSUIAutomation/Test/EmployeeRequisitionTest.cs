﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using HRMSUIAutomation.Utilities;
using HRMSUIAutomation.Page;

using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;




namespace HRMSUIAutomation.Test
{
    [TestClass]
    public class EmployeeRequisitionTest 
    {
        private IWebDriver driver;
        LogInPage logInPage;
        DashboardPage dashboardPage;
        EmployeeRequisitionPage employeeRequisitionPage;
        

        [TestInitialize]
        public void TestInitialize()
        {
            driver = TestBrowser.InitializeDriver();

            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(30));
            driver.Navigate().GoToUrl(TestURL.URL);
            driver.Manage().Window.Maximize();

            logInPage = new LogInPage(driver);
            PageFactory.InitElements(driver, logInPage);

            dashboardPage = new DashboardPage(driver);
            PageFactory.InitElements(driver, dashboardPage);

            employeeRequisitionPage = new EmployeeRequisitionPage(driver);
            PageFactory.InitElements(driver, employeeRequisitionPage);

            //Log in to applicaiton
            logInPage.ValidLogIn("hr", "123");

        }


        //  TC/EmplRequisitionList/22 	Verify that employee requisition can do with the available field
        //  Author : TareQ
        [Description("Verify that employee requisition can do with the available field")]
        [TestCategory("EmployeeRequisition")]
        [Owner("TareQ")]
        [Priority(1)]
        [TestMethod]

        public void EmployeeRequisitionCanDoWithAvailableField()
        {
            //Navigate to Employee requition page
            dashboardPage.NavigateToEmployeeRequisitionForm();

            //Save Requisiton
            employeeRequisitionPage.EmployeeRequisition("Software QA Engineer", 2, "Brac IT", "12312", "27/07/2015");

        }


        //  TC/EmplRequisitionList/06 	Verify that multiple requisition can be applied by using "Designation" and "Required Employee"
        //  Author : TareQ
        [Description("Verify that multiple requisition can be applied by Designation and Required Employee")]
        [TestCategory("EmployeeRequisition")]
        [Owner("TareQ")]
        [Priority(2)]
        [TestMethod]

        public void MultipleRequisitionCanBeAppliedUsingDesignation()
        {
            //Navigate to Employee requition page
            dashboardPage.NavigateToEmployeeRequisitionForm();

            //Save Requisiton
            employeeRequisitionPage.FirstEmployeeRequisition("Software QA Engineer", 2, "Brac IT", "29/07/2015");
            employeeRequisitionPage.SecondEmployeeRequisition("Technical Writer", 5, "Brac IT", "12312", "27/07/2015");
            

        }



        //TC/EmplRequisitionApplication/03	Verify that "Version" is changed after saving an edited form
        //Author : TareQ
        [Description("Verify that Version is changed after saving an edited form")]
        [TestCategory("EmployeeRequisition")]
        [Owner("TareQ")]
        [Priority(3)]
        [TestMethod]
        public void VersionChangedAfterSavingEditedForm()
        {         

            //Navigate to DashboradPage
            dashboardPage.NavigateToEmployeeRequisitionForm();

            //Enter code in code position
            employeeRequisitionPage.CodeInput("4");

        }

        //TC/EmplRequisitionApplication/09	Verify that clicking on "Clear" button discard the operation and refresh the form
        //Author : TareQ
        [Description("Verify that clicking on Clear button discard the operation and refresh the form")]
        [TestCategory("EmployeeRequisition")]
        [Owner("TareQ")]
        [Priority(4)]
        [TestMethod]

        public void ClearButtonDiscardOperation()
        {
            
            driver.SwitchTo().DefaultContent();

            //Navigate to DashboradPage
            dashboardPage.NavigateToEmployeeRequisitionForm();

            //Enter code in code position
            employeeRequisitionPage.CodeInputForClear("4");

           
        }


        //TC//EmplRequisitionList/01	Verify that error message is displayed for mandatory field "Designation"
        //Author : TareQ
        [Description("Verify that error message is displayed for mandatory field Designation")]
        [TestCategory("EmployeeRequisition")]
        [Owner("TareQ")]
        [Priority(5)]
        [TestMethod]
        public void ErrorMessageDisplayForMandatoryFieldDesignation()
        {
       
            //Navigate to DashboradPage
            dashboardPage.NavigateToEmployeeRequisitionForm();
            //Send mandatory values
            employeeRequisitionPage.MandatoryFieldsToTestDesignation(5);

            //Verify mandatory field Designation
            Assert.AreEqual("This field is required.", employeeRequisitionPage.ErrorMessageForMandatoryFieldDesignation());

        }


        //TC/EmplRequisitionList/03 	Verify that error message is displayed for mandatory field "Required Employee"
        //Author : TareQ
        [Description("Verify that error message is displayed for mandatory field Required Employee")]
        [TestCategory("EmployeeRequisition")]
        [Owner("TareQ")]
        [Priority(6)]
        [TestMethod]
        public void ErrorMessageDisplayForMandatoryFieldRequiredEmployee()
        {           

            //Navigate to DashboradPage
            dashboardPage.NavigateToEmployeeRequisitionForm();
            //Send mandatory values
            employeeRequisitionPage.MandatoryFieldsToTestRequiredEmployee("Software QA Engineer");
            //Verify mandatory field Required Employee
            Assert.AreEqual("This field is required.", employeeRequisitionPage.ErrorMessageForMandatoryFieldRequiredEmployee());

        }




        //TC//EmplRequisitionList/16	Verify that error message is displayed for mandatory field "Company"
        //Author : TareQ
        [Description("Verify that error message is displayed for mandatory field Company")]
        [TestCategory("EmployeeRequisition")]
        [Owner("TareQ")]
        [Priority(7)]
        [TestMethod]
        public void ErrorMessageDisplayForMandatoryFieldCompany()
        {

            //Navigate to DashboradPage
            dashboardPage.NavigateToEmployeeRequisitionForm();
            //Send mandatory values
            employeeRequisitionPage.MandatoryFieldsToTestCompany("Software QA Engineer", 5, "27/07/2015");
            //Verify mandatory field Required Employee
            Assert.AreEqual("Please fill the required field.", employeeRequisitionPage.ErrorMessageForMandatoryFieldCompany());

        }


        //TC//EmplRequisitionList/18	Verify that error message is displayed for mandatory field "Joining Date"
        //Author : TareQ
        [Description("Verify that error message is displayed for mandatory field Company")]
        [TestCategory("EmployeeRequisition")]
        [Owner("TareQ")]
        [Priority(7)]
        [TestMethod]

        public void ErrorMessageDisplayForMandatoryFieldJoiningDate()
        {

            //Navigate to DashboradPage
            dashboardPage.NavigateToEmployeeRequisitionForm();
            //Send mandatory values
            employeeRequisitionPage.MandatoryFieldsToTestJoiningDate("Software QA Engineer", 5, "Brac IT");
            //Verify mandatory field Joining Date
            Assert.AreEqual("Please fill the required field.", employeeRequisitionPage.ErrorMessageForMandatoryFieldJoiningDate());

        }


        //TC//EmplRequisitionList/18	Verify that error message is displayed for mandatory field "Reference"
        //Author : TareQ
        [Description("Verify that error message is displayed for mandatory field Reference")]
        [TestCategory("EmployeeRequisition")]
        [Owner("TareQ")]
        [Priority(8)]
        [TestMethod]
        public void ErrorMessageDisplayForMandatoryFieldReference()
        {

            //Navigate to DashboradPage
            dashboardPage.NavigateToEmployeeRequisitionForm();
            //Send mandatory values
            employeeRequisitionPage.MandatoryFieldsToTestReference("Software QA Engineer", 5, "Brac IT", "12546");
            //Verify mandatory field Reference
            Assert.AreEqual("Please fill the required field.", employeeRequisitionPage.ErrorMessageForMandatoryFieldReference());

        }

    }
}
