﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;


namespace HRMSUIAutomation.Utilities
{
    public static class TestBrowser
    {
        
        public static IWebDriver InitializeDriver ()
        {
            IWebDriver driver = null;
            
            string browserName = ConfigurationManager.AppSettings["Browser"].ToString();

            switch (browserName)
            {
                case "Firefox":
                driver = new FirefoxDriver();
                break;
            }
            
            return driver;

		//System.setProperty("webdriver.chrome.driver", "C:\\NanoISV\\chromedriver_win32\\chromedriver.exe");
		//driver = new ChromeDriver();
			
		//C:\Development\ExosTest\IE Driver
		//System.setProperty("webdriver.ie.driver", "C:\\Development\\ExosTest\\IE Driver\\IEDriverServer.exe");
		
		//driver=new InternetExplorerDriver();
        }
    }
}
