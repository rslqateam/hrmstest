﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace HRMSUIAutomation.Utilities
{
    public static class TestURL
    {
        //private String _getURL;

        public static String URL
        {
            get
            {
                return ConfigurationManager.AppSettings["ApplicationURL"].ToString(); //"http://172.16.1.47/HRMS/(S(a2401k2w1v2vlsjchmwlwhx4))/Login.aspx";
               
            }
        }

    }
}
