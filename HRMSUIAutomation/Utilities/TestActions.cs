﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OpenQA.Selenium;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System.Windows.Forms;
using System.Threading;

namespace HRMSUIAutomation.Utilities
{
    public static class TestActions
    {
        public static void click(IWebElement element)
        {
            //driver.findElement(locator).click();	
            element.Click();
            
        }

        public static void typeText(this IWebElement element, String inputText)
        {
            if (element.Displayed)
            {
                element.Clear();
                element.SendKeys(inputText);
            }
        }

        //Clear text box

        public static void removeText(this IWebElement element)
        {
            if (element.Displayed)
            {
                element.Clear();
            }
        }


        ////Select by visible text
        //public void selectByVisibleText(IWebDriver driver, By locator, String visibleText)
        //{
        //    IWebElement elSelectData = driver.FindElement(locator);
        //    Select  selectData = new Select(elSelectData);
        //    selectData.selectByVisibleText(visibleText);
        //}

        //Select by Visible text
        public static void selectByVisibleText(this IWebElement element, String visibleText)
        {
            SelectElement selectElement = new SelectElement(element);
            selectElement.SelectByText(visibleText);
        }

        //Select by Value
        public static void selectByValue(this IWebElement element, String value)
        {
            SelectElement selectElement = new SelectElement(element);
            selectElement.SelectByValue(value);
        }

        ////Select by Value
        //public void selectByValue(WebElement element, String value)
        //{
        //    //WebElement elSelectData = driver.findElement(locator);
        //    Select selectData = new Select(element);
        //    selectData.selectByValue(value);
        //}


        ////Select by index
        //public void selectByIndex(WebDriver driver, By locator, int index)
        //{
        //    WebElement elSelectData = driver.findElement(locator);
        //    Select selectData = new Select(elSelectData);
        //    selectData.selectByIndex(index);
        //}

        //Select by index
        public static void selectByIndex(this IWebElement element, int index)
        {
            SelectElement selectElement = new SelectElement(element);
            selectElement.SelectByIndex(index);
        }


        //Return Text
        public static String returnText(this IWebElement element)
        {
            return element.Text.ToString();
        }

        //Click Sub menu

        public static void clickSubMenu(IWebDriver driver, By mainMenu, By SubMenu)
        {
            Actions action = new Actions(driver);
            action.MoveToElement(driver.FindElement(mainMenu)).Build().Perform();
            click(GetWebElemnt(driver, SubMenu));
        }

        public static IWebElement GetWebElemnt(IWebDriver driver, By locator)
        {
            return driver.FindElement(locator);

        }

        public static bool IsOptionButtonSelected(this IWebElement element)
        {
            SelectElement selectElement = new SelectElement(element);
            return selectElement.SelectedOption.Selected;
        }

        public static string returnListValueByIndex(this IWebElement element, int index)
        {
            SelectElement selectElement = new SelectElement(element);
            IList<IWebElement> list = selectElement.Options;
            return list[index].Text;

        }

        public void FileUpload(string FilePath)
        {
            SendKeys.SendWait(FilePath);
            Thread.Sleep(3000);
            SendKeys.SendWait(@"{Enter}");
        }
    }
}
