﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using OpenQA.Selenium;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;
//using org.openqa.selenium.Point;

using HRMSUIAutomation.Utilities;

namespace HRMSUIAutomation.Page
{
   //[TestClass]
    class CreateEmployeeRequisitionPageUS1
    {
        private IWebDriver driver;
        
        public  CreateEmployeeRequisitionPageUS1(IWebDriver driver)
        {
            this.driver = driver;
        }

        //User Story 1 : (Create Employee Requisition)
        //Author : Kana

        //Select Designation
        [FindsBy(How = How.Id, Using = "ddlDesig")]
        private IWebElement DesignationSelect { get; set; }

        //Enter Required Employee
        [FindsBy(How = How.Id, Using = "txtEmpId")]
        private IWebElement RequiredEmployeeEnter { get; set; }

        //Click Add button
        [FindsBy(How = How.Id, Using = "Button1")]
        private IWebElement AddButtonClick { get; set; }

        

        
        ////Click Employee Requisition (Designation) to Expand 
        //[FindsBy(How = How.LinkText, Using = des)]
        //private IWebElement ClickDesignationToExpand { get; set; }

        //Select Company for Employee Requisition Application
        [FindsBy(How = How.Id, Using = "ddlCompany1")]
        private IWebElement CompanySelectForEmployeeRequisition { get; set; }

        //Select Business Unit for Employee Requisition Application
        [FindsBy(How = How.Id, Using = "ddlBusinessUnit1")]
        private IWebElement BusinessUnitSelectForEmployeeRequisition { get; set; }

        //Select Department for Employee Requisition Application
        [FindsBy(How = How.Id, Using = "ddlDepartment1")]
        private IWebElement DepartmentSelectForEmployeeRequisition { get; set; }

        //Select Section for Employee Requisition Application
        [FindsBy(How = How.Id, Using = "ddlSection1")]
        private IWebElement SectionSelectForEmployeeRequisition { get; set; }

        //Enter No. of Employee field for Employee Requisition Application
        [FindsBy(How = How.Id, Using = "txtEmployeeNo1")]
        private IWebElement NoOfEmployeeForEmployeeRequisition { get; set; }

        //Enter Job Responsibility field for Employee Requisition Application
        [FindsBy(How = How.Id, Using = "txtJobRespons1")]
        private IWebElement JobResponsibilityForEmployeeRequisition { get; set; }

        //Select Job Nature for Employee Requisition Application
        [FindsBy(How = How.Id, Using = "ddljobNature1")]
        private IWebElement JobNatureForEmployeeRequisition { get; set; }

        //Enter Job Qualification for Employee Requisition Application
        [FindsBy(How = How.Id, Using = "txtQualif1")]
        private IWebElement JobQualificationForEmployeeRequisition { get; set; }

        //Enter Job Experience for Employee Requisition Application
        [FindsBy(How = How.Id, Using = "txtexperience1")]
        private IWebElement JobExperienceForEmployeeRequisition { get; set; }

        //Enter Additional Requirement for Employee Requisition Application
        [FindsBy(How = How.Id, Using = "txtAddRequire1")]
        private IWebElement AdditionalRequirementForEmployeeRequisition { get; set; }

        //Enter Salary Range for Employee Requisition Application
        [FindsBy(How = How.Id, Using = "txtSalary1")]
        private IWebElement SalaryRangeForEmployeeRequisition { get; set; }

        //Enter Other Benefit for Employee Requisition Application
        [FindsBy(How = How.Id, Using = "txtBenefits1")]
        private IWebElement OtherBenefitForEmployeeRequisition { get; set; }

        //Enter Job Location for Employee Requisition Application
        [FindsBy(How = How.Id, Using = "txtJobLocation1")]
        private IWebElement JobLocationForEmployeeRequisition { get; set; }

        //Enter Joining Date for Employee Requisition Application
        [FindsBy(How = How.Id, Using = "txtJoiningDate1")]
        private IWebElement JoiningDateForEmployeeRequisition { get; set; }

        //Enter Gender for Employee Requisition Application
        [FindsBy(How = How.Id, Using = "ddlGender1")]
        private IWebElement GenderForEmployeeRequisition { get; set; }

        //Enter Age for Employee Requisition Application
        [FindsBy(How = How.Id, Using = "txtAge1")]
        private IWebElement AgeForEmployeeRequisition { get; set; }

        //Enter Remarks for Employee Requisition Application
        [FindsBy(How = How.Id, Using = "txtRemarks1")]
        private IWebElement RemarksForEmployeeRequisition { get; set; }

        //Enter Reference for Employee Requisition Application
        [FindsBy(How = How.Id, Using = "txtRefference")]
        private IWebElement ReferenceForEmployeeRequisition { get; set; }

        //Click Save button for Employee Requisition Application
        [FindsBy(How = How.Id, Using = "btnSave")]
        private IWebElement SaveButtonClick { get; set; }

        //Click "Yes, I am"  for Employee Requisition Application
        [FindsBy(How = How.XPath, Using = "//div[3]/button")]
        private IWebElement YesButtonClick { get; set; }

        //Click "To"  for Employee Requisition Application Approval send
        [FindsBy(How = How.XPath, Using = "//div[3]/button")]
        private IWebElement ToClick { get; set; }


        //Enter "To Name Select"  for Employee Requisition Application Approval send
        [FindsBy(How = How.Id, Using = "ddlEmployee")]
        private IWebElement ToNameSelect { get; set; }

        //Enter "Note"  for Employee Requisition Application Approval send
        [FindsBy(How = How.Id, Using = "txtNote")]
        private IWebElement NoteEnter { get; set; }

        

        public void EmployeeRequisitionApplicationForDesignation(string Designation)
        {
            //Employee Requisition for Designation
            //DesignationSelect.SendKeys(Designation);
           
            TestActions.selectByVisibleText(DesignationSelect, Designation);
        }
        public void EmployeeRequisitionApplicationForRequiredEmployee(string RequiredEmployee)
        {
            //Enter Required Employee
            RequiredEmployeeEnter.SendKeys(RequiredEmployee);
            //TestActions.selectByVisibleText(RequiredEmployeeEnter, RequiredEmployee);


        }
        public void AddButtonClickForEmployeeRequisitionApplication()
        {
            AddButtonClick.Click();
        }
        public void EmployeeRequisitionApplicationToExpand(string Designation)
        {
            string des = "Designation: " + Designation;
            driver.FindElement(By.LinkText(des)).Click();
            
        }
        public void EmployeeRequisitionApplicationForInputField(string Company, string BusinessUnit, string Department, string Section, string JobResponsibility, string JobNature, string Qualification, string Experiance, string AdditionalRequirement, string Salary, string OtherBenefit, string JobLocation, string JoiningDate, string Gender, string Age, string Remarks, string Reference)
        {
            TestActions.selectByVisibleText(CompanySelectForEmployeeRequisition, Company);
            TestActions.selectByVisibleText(BusinessUnitSelectForEmployeeRequisition, BusinessUnit);
            TestActions.selectByVisibleText(DepartmentSelectForEmployeeRequisition, Department);
            TestActions.selectByVisibleText(SectionSelectForEmployeeRequisition, Section);
            JobResponsibilityForEmployeeRequisition.SendKeys(JobResponsibility);
            TestActions.selectByVisibleText(JobNatureForEmployeeRequisition, JobNature);
            JobQualificationForEmployeeRequisition.SendKeys(Qualification);
            JobExperienceForEmployeeRequisition.SendKeys(Experiance);
            AdditionalRequirementForEmployeeRequisition.SendKeys(AdditionalRequirement);
            SalaryRangeForEmployeeRequisition.SendKeys(Salary);
            OtherBenefitForEmployeeRequisition.SendKeys(OtherBenefit);
            JobLocationForEmployeeRequisition.SendKeys(JobLocation);
            JoiningDateForEmployeeRequisition.SendKeys(JoiningDate);
            TestActions.selectByVisibleText(GenderForEmployeeRequisition, Gender);
            AgeForEmployeeRequisition.SendKeys(Age);
            RemarksForEmployeeRequisition.SendKeys(Remarks);
            ReferenceForEmployeeRequisition.SendKeys(Reference);
        }

        public void SaveButtonClickForEmployeeRequisitionApplication()
        {
            SaveButtonClick.Click();
        }
        public void YesButtonClickForEmployeeRequisitionApplication()
        {
            YesButtonClick.Click();
        }


        public void SubmitEmployeeRequisitionForApproval(string ToName, string Note)
        {
           // driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(30));
            
           // driver.SwitchTo().Window("Raven|Approval System");
            driver.SwitchTo().Window(driver.WindowHandles.Last());
           
           // driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(30));

           

            ToClick.Click();
            ToNameSelect.SendKeys(ToName);
            NoteEnter.SendKeys(Note);

        }
    }
}
