﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using OpenQA.Selenium;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.PageObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HRMSUIAutomation.Utilities;

namespace HRMSUIAutomation.Page
{
    [TestClass]
    public class GradeDesignationMapPage 
    {
        private IWebDriver driver;

        public GradeDesignationMapPage(IWebDriver driver)
        {
		    this.driver = driver;
	    }

        // Find  Company
        [FindsBy(How = How.Id, Using = "ddlCompany")]
        private IWebElement CompanyPosition { get; set; }
        // Find  Designation
        [FindsBy(How = How.Id, Using = "ddlDesignation")]
        private IWebElement DesignationPosition { get; set; }
        // Find  GradeType
        [FindsBy(How = How.Id, Using = "ddlGradeType")]
        private IWebElement GradeTypePosition { get; set; }
        // Find  ddlGrade
        [FindsBy(How = How.Id, Using = "ddlGrade")]
        private IWebElement GradePosition { get; set; }
        // Find  Remarks
        [FindsBy(How = How.Id, Using = "txtRemarks")]
        private IWebElement RemarksPosition { get; set; }
        // Find  Save Button
        [FindsBy(How = How.Id, Using = "Button1")]
        private IWebElement SaveButton { get; set; }
        // Find  Success message
        [FindsBy(How = How.XPath, Using = "//body/div/div/div[2]")]
        private IWebElement SuccessMessagePosition { get; set; }

        //TC/GradeDesinationMap/001    Verify that "Code" is auto generated from database after saving information
        //Author : TareQ

        public void AllInputFieldsforGradeDesignationMap(string Company, string Designation, string GradeType, string Grade, string Remarks)
        {
            // Enter Company
            TestActions.selectByVisibleText(CompanyPosition, Company);
            // Enter Designation
            TestActions.selectByVisibleText(DesignationPosition, Designation);
            //Enter GradeType 
            TestActions.selectByVisibleText(GradeTypePosition, GradeType);
            //Enter Grade
            TestActions.selectByVisibleText(GradePosition, Grade);
            //Enter Remarks
            TestActions.typeText(RemarksPosition, Remarks);
            //save button click
            SaveButton.Click();
        }
        public string CheckSuccessMessage()
        {
            string SuccessMessage = SuccessMessagePosition.Text.ToString();
            return SuccessMessage;
        }
    }
}
