﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using OpenQA.Selenium;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;
using HRMSUIAutomation.Utilities;

using System.Windows.Forms;


namespace HRMSUIAutomation.Page
{
    class EmployeeInformationPage 
    {
        private IWebDriver driver;

        //CommonAction commonAction=new CommonAction();
      
    
         public EmployeeInformationPage(IWebDriver driver)
        {
		    this.driver = driver;
	    }

       

         //Save button
         [FindsBy(How = How.Id, Using = "btnSave")]
         private IWebElement SaveButton { get; set; }

       

        //for Name Field
         [FindsBy(How = How.XPath, Using = "//div[2]/div/div/div/div[2]")]
         private IWebElement ErrorMessageName { get; set; }

        //Method displaying error message for "Name" field
         public string ErrorMessageForMandatoryFieldName()
         {
                return ErrorMessageName.Text.ToString();
             
         }

         //for Religion Field
         [FindsBy(How = How.XPath, Using = "//div[4]/div[2]/div/div/div[2]")]
         private IWebElement ErrorMessageReligion { get; set; }

         //Method displaying error message for "Religion" field
         public string ErrorMessageForMandatoryFieldReligion() 
         {
             return ErrorMessageReligion.Text.ToString();
             
         }

         //for Date of Birth Field
         [FindsBy(How = How.XPath, Using = "//div[@id='tab_1_1']/div[2]/div[3]/div/div/div[2]")]
         private IWebElement ErrorMessageDateOfBirth { get; set; }

         //Method displaying error message for "Date of Birth"  field
        public string ErrorMessageForMandatoryFieldDateOfBirth()
         {
             return ErrorMessageDateOfBirth.Text.ToString();
            
         }

        //Contact Info Click
        [FindsBy(How = How.LinkText, Using = "Contact Info")]
        private IWebElement ContactInfo { get; set; }

        //for Present Address
        [FindsBy(How = How.XPath, Using = "//div[2]/div/div/div/div[2]/div/div/div/div/div[2]")]
        private IWebElement ErrorMessagePresentAddress { get; set; }
        
        //Method For Displaying  Error Message for Mendatory Field Present Address 
        public string ErrorMessageForMendatoryFieldPresentAddress()
        {
            return ErrorMessagePresentAddress.Text.ToString();
        }


        //for Parmanent Address
        [FindsBy(How = How.XPath, Using = "//div[@id='tab_1_2']/div[3]/div/div/div/div[2]")]
        private IWebElement ErrorMessagePermanentAddress { get; set; }

        //Method For Displaying  Error Message for Mendatory Field Permanent Address 
        public string ErrorMessageForMendatoryFieldPermanentAddress()
        {
            return ErrorMessagePermanentAddress.Text.ToString();
            
        }

        //Job details Click
        [FindsBy(How = How.LinkText, Using = "Job Details")]
        private IWebElement JobDetails { get; set; }

        //for Company
        [FindsBy(How = How.XPath, Using = "//div/div/div/div[3]/div/div/div/div/div[2]")]
        private IWebElement ErrorMessageCompany { get; set; }

        //Method For Displaying  Error Message for Mendatory Field Company 
        public string ErrorMessageForMandatoryFieldCompany()
        {
            return ErrorMessageCompany.Text.ToString();
           
        }


        //for Department
        [FindsBy(How = How.XPath, Using = "//div/div[3]/div/div/div[2]")]
        private IWebElement ErrorMessageDepartment { get; set; }

        //Method For Displaying  Error Message for Mendatory Field Department 
        public string ErrorMessageForMandatoryFieldDepartment()
        {
            return ErrorMessageDepartment.Text.ToString();
        } 
 

        //for Section
        [FindsBy(How = How.XPath, Using = "//div/div/div/div[3]/div[2]/div/div/div/div[2]")]
        private IWebElement ErrorMessageSection { get; set; }

        //Method For Displaying  Error Message for Mendatory Field Section
        public string ErrorMessageForMandatoryFieldSection()
        {
            return ErrorMessageSection.Text.ToString();

        }


        //for Designation
        [FindsBy(How = How.XPath, Using = "//div[3]/div[3]/div/div/div/div[2]")]
        private IWebElement ErrorMessageDesignation { get; set; }

        //Method For Displaying  Error Message for Mendatory Field Designation
        public string ErrorMessageForMandatoryFieldDesignation()
        {
            return ErrorMessageDesignation.Text.ToString();
        }


        //for Attendance Group
        [FindsBy(How = How.XPath, Using = "//div[3]/div[2]/div/div/div[2]")]
        private IWebElement ErrorMessageAttendanceGroup { get; set; }

        //Method For Displaying  Error Message for Mendatory Field Attendance Group
        public string ErrorMessageForMandatoryFieldAttendanceGroup()
        {
            return ErrorMessageAttendanceGroup.Text.ToString();
        }

        //for Joining Date
        [FindsBy(How = How.XPath, Using = "//div[3]/div[2]/div/div/div[2]")]
        private IWebElement ErrorMessageJoiningDate { get; set; }

        //Method For Displaying  Error Message for Mendatory Field Joining Date
        public string ErrorMessageForMandatoryFieldJoiningDate()
        {
            string errorMessage = ErrorMessageJoiningDate.Text.ToString();
            return errorMessage;
        }



        //Others Click
        [FindsBy(How = How.LinkText, Using = "Others")]
        private IWebElement Others { get; set; }


        //for Weekly Holiday
        [FindsBy(How = How.XPath, Using = "//div[4]/div/div/div/div[2]/div[2]")]
        private IWebElement ErrorMessageWeeklyHoliday { get; set; }

        //Method For Displaying  Error Message for Mendatory Field Weekly Holiday
        public string ErrorMessageForMandatoryFieldWeeklyHoliday()
        {
            return ErrorMessageWeeklyHoliday.Text.ToString();
        }

        
        //TC/Personal/001	Verify that a new "Employee" can be created using available fields
        //Author : Kana

        //for Name Field
        [FindsBy(How = How.Id, Using = "txtName")]
        private IWebElement NameEnter { get; set; }

        //for Religion Field

        [FindsBy(How = How.Id, Using = "ddlReligion")]
        private IWebElement ReligionSelect { get; set; }

        //for DateOfBirth Field

        [FindsBy(How = How.Id, Using = "txtDoB")]
        private IWebElement DateOfBirthEnter { get; set; }

        //Method for input Name,Religion,DateOfBirth
        public void PersonalTabInput(string Name, string Religion, string DateOfBirth)
        {
            //Enter Name,Religion,DateOfBirth
            TestActions.typeText(NameEnter,Name);
            TestActions.selectByVisibleText(ReligionSelect, Religion);
            TestActions.typeText(DateOfBirthEnter, DateOfBirth);
        }


        //for Present Address Field

        [FindsBy(How = How.Id, Using = "txtPreAddressEng")]
        private IWebElement PresentAddressEnter { get; set; }

       
        //for Permanent Address Field

        [FindsBy(How = How.Id, Using = "txtPerAddressEng")]
        private IWebElement PermanentAddressEnter { get; set; }

        //Method for input Present Address,Permanent Address
        public void ContactInfoTabInput(string PresentAddress, string PermanentAddress)
        {
            //Enter PresentAddress,PermanentAddress
            TestActions.typeText(PresentAddressEnter, PresentAddress);
            TestActions.typeText(PermanentAddressEnter, PermanentAddress);
       
        }

        
        // For Company Select
        [FindsBy(How = How.Id, Using = "ddlCompany")]
        private IWebElement CompanySelect { get; set; }

        // For Business Unit Select
        [FindsBy(How = How.Id, Using = "ddlBUnit")]
        private IWebElement BusinessUnitSelect { get; set; }

        // For Department Select
        [FindsBy(How = How.Id, Using = "ddlDept")]
        private IWebElement DepartmentSelect { get; set; }


        // For Section Select
        [FindsBy(How = How.Id, Using = "ddlSection")]
        private IWebElement SectionSelect { get; set; }


        // For Designation Select
        [FindsBy(How = How.Id, Using = "ddlDesig")]
        private IWebElement DesignationSelect { get; set; }


        // For Attendance Group Select
        [FindsBy(How = How.Id, Using = "ddlAttenGroup")]
        private IWebElement AttendanceGroupSelect { get; set; }


        // For Joining Date Select
        [FindsBy(How = How.Id, Using = "txtJoinDate")]
        private IWebElement JoiningDateSelect { get; set; }

        // For Employee Selection Id

        [FindsBy(How = How.Id, Using = "txtSelecId")]
        private IWebElement SelectionIDTextBox { get; set; }

        //Error Message For Invalid Selection Id

        [FindsBy(How = How.XPath, Using = "//body/div/div/div[2]")]
        private IWebElement ErrorMessageInvalidEmployeeSelectionId { get; set; }


        //public void JobDetailsTabInputCompany(string Company)
        //{
        //    //Enter Company
        //    CompanyEnter.SendKeys(Company);

        //}
        //public void JobDetailsTabInputDepartment(string Department)
        //{
        //    //Enter Department
        //    DepartmentEnter.SendKeys(Department);

        //}

        //public void JobDetailsTabInputSection(string Section)
        //{
        //    //Enter Section

        //    SectionEnter.SendKeys(Section);

        //}
        //public void JobDetailsTabInputDesignation(string Designation)
        //{
        //    //Enter Designation
        //    DesignationEnter.SendKeys(Designation);
        //}
        //public void JobDetailsTabInputAttendanceGroup(string AttendanceGroup)
        //{
        //    //Enter Attendance Group
        //    AttendanceGroupEnter.SendKeys(AttendanceGroup);
        //}
        //public void JobDetailsTabInputJoiningDate(string JoiningDate)
        //{
        //    //Enter Joining Date
        //    JoiningDateEnter.SendKeys(JoiningDate);
        //}
       
        ////Others Click for entering input
        //[FindsBy(How = How.LinkText, Using = "Others")]
        //private IWebElement OthersForInput { get; set; }

        //public void OthersClickForInput()
        //{

        //    OthersForInput.Click();
        //}

        //// For Holiday Select
        //[FindsBy(How = How.XPath, Using = "//div/div/div/ul/li/div")]
        //private IWebElement HolidayEnter { get; set; }
        //public void OthersTabInput(string Holidays)
        //{
        //    //Enter Holidays
        //    CompanyEnter.SendKeys(Holidays);
 
        //}



        //TC/Personal/008	Verify that error message is displayed for invalid selection ID
        //Author : Susmita Kar
  
        //Method for entering "Employee Selection Id" and pressing enter
       
        public void SelectedEmployeeSearch(string EmployeeSelectionId)
        {

            TestActions.typeText(SelectionIDTextBox, EmployeeSelectionId);
            //SelectionIDTextBox.SendKeys(Keys.Enter);
        }

        //Method for displaying error message for invalid "Employee Selection Id"
        public string ErrorMessageForInvalidSelectionIdText()
        {
            return ErrorMessageInvalidEmployeeSelectionId.Text.ToString();
        }


        //TC/Personal/007	Verify that employee information can be searched by Selection ID
        //Author : Susmita Kar
       
        //Name Field for Personal Tab

        [FindsBy(How = How.Id, Using = "txtName")]
        public IWebElement EmployeeNameTextBox { get; set; }

        public string ReturnEmployeeName()
        {

            return EmployeeNameTextBox.GetAttribute("value");
        }


       
        //For Father's Name Field for Personal Tab

        [FindsBy(How = How.Name,Using = "txtEmpFatherName")]
        private IWebElement FathersNameTextBox { get; set; }


        public string ReturnFatherName()
        {
           return FathersNameTextBox.GetAttribute("value");
        }


        
        //For Mother's Name Field for Personal Tab

        [FindsBy(How = How.Name, Using = "txtEmpMotherName")]
        private IWebElement MotherNameTextBox { get; set; }


        public string ReturnMotherName()
        {
            return MotherNameTextBox.GetAttribute("value");
        }



       
        //Present Address Field for Contact Info Tab

        [FindsBy(How = How.Id, Using = "txtPreAddressEng")]
        private IWebElement PresentAddressTestBox { get; set; }


        public string ReturnPresentAddress()
        {
            return PresentAddressTestBox.GetAttribute("value");
          
        }


        
        //Permanent Address Field for Contact Info Tab

        [FindsBy(How = How.Id, Using = "txtPerAddressEng")]
        private IWebElement PermanentAddressTextBox { get; set; }


        public string ReturnPermanentAddress()
        {
            return PermanentAddressTextBox.GetAttribute("value");
           
        }

        
        //E-Mail Field for Contact Info Tab

        [FindsBy(How = How.Name, Using = "txtEmail")]
        private IWebElement EmailTextBox { get; set; }


        public string ReturnEmailAddress()
        {
            return EmailTextBox.GetAttribute("value");
        }





        //Company Field for Contact Info Tab

        [FindsBy(How = How.Id, Using = "ddlCompany")]
        private IWebElement CompanyTextBox { get; set; }


        public string Returncompany()
        {
            //return CompanyTextBox.GetAttribute("value");
            return CompanyTextBox.Text.ToString();
        }


        //Business Unit Field for Contact Info Tab

        [FindsBy(How = How.Id, Using = "ddlBUnit")]
        private IWebElement BusinessUnitTextBox { get; set; }


        public string ReturnBusinessUnit()
        {
            //return BusinessUnitTextBox.GetAttribute("value");
            return BusinessUnitTextBox.Text.ToString();
        }

        //Department Field for Contact Info Tab

        [FindsBy(How = How.Id, Using = "ddlDept")]
        private IWebElement DepartmentTextBox { get; set; }


        public string ReturnDepartment()
        {
            //return DepartmentTextBox.GetAttribute("value");
            return DepartmentTextBox.Text.ToString();
        }


        //Section Field for Contact Info Tab

        [FindsBy(How = How.Id, Using = "txtPerAddressEng")]
        private IWebElement SectionTextBox { get; set; }


        public string ReturnSection()
        {
            //return SectionTextBox.GetAttribute("value");
            return SectionTextBox.Text.ToString();
        }

        
        //Designation Field for Contact Info Tab

        [FindsBy(How = How.Id, Using = "ddlDesig")]
        private IWebElement DesignationTextBox { get; set; }


        public string ReturnDesignation()
        {
            //return DesignationTextBox.GetAttribute("value");
            return DesignationTextBox.Text.ToString();
        }


        //Joining Date Field for Contact Info Tab

        [FindsBy(How = How.Id, Using = "txtJoinDate")]
        private IWebElement JoiningDateTextBox { get; set; }


        public string ReturnJoiningDate()
        {
            return JoiningDateTextBox.GetAttribute("value");
        }


        //TC/Personal/024 Verify that error message is displayed for mandatory field "DateOfBirth"
        //Author : Susmita Kar

        public void EmployeeSetupFormInputWithoutDateOfBirth(string Name, string Religion,string PresentAddress, string PermanentAddress, string Company, string Department, string Section, string Designation, string AttendenceGroup, string JoiningDate)
        {
            //Input Personal Tab Mendatory Field without DateOfBirth
            TestActions.typeText(NameEnter, Name);
            TestActions.selectByVisibleText(ReligionSelect, Religion);
           
            //Click Contact Information
            ContactInfo.Click();

            //Input Contact Info Tab Mendatory Field
            TestActions.typeText(PresentAddressEnter, PresentAddress);
            TestActions.typeText(PermanentAddressEnter, PermanentAddress);

            //Click Job Details
            JobDetails.Click();

            //Input Job Details Tab Mendatory Field
            TestActions.selectByVisibleText(CompanySelect, Company);
            TestActions.selectByVisibleText(DepartmentSelect, Department);
            TestActions.selectByVisibleText(SectionSelect, Section);
            TestActions.selectByVisibleText(DesignationSelect, Designation);
            TestActions.selectByVisibleText(AttendanceGroupSelect, AttendenceGroup);
            TestActions.typeText(JoiningDateSelect, JoiningDate);
            SaveButton.Click();

        }



        //TC/Personal/30	Verify that error message is displayed if uploaded image format is other than JPG,PNG,JPEG
        //Author : Susmita Kar



        // For Uploading Image with invalid formate

        [FindsBy(How = How.Id, Using = "Img1")]
        private IWebElement InvalidFormatImageUpload { get; set; }

        public void ImageUploadOfInvalidFormate(string UploadedImage)
        {
            //Click Others
            Others.Click();

            //Upload Image of Invalid Format
            InvalidFormatImageUpload.Click();

            SendKeys.SendWait(UploadedImage);
            //Thread.Sleep(5000);
            SendKeys.SendWait(@"{Enter}");


        }



        //Error Message For Uploading Image of Other Format Like Doc

        [FindsBy(How = How.CssSelector, Using = "div.toast-message")]
        private IWebElement ErrorMessageInvalidFormatImage { get; set; }


       public string ErrorMessageForInvalidFormatImageUpload()
        {
            return ErrorMessageInvalidFormatImage.Text.ToString();
        }



        //TC/Personal/029	Verify that error message is displayed if uploaded image is greater than 1 MB
        //Author : Susmita Kar



        // For Image field
        [FindsBy(How = How.Id, Using = "Img1")]
        private IWebElement ImageUpload { get; set; }

        //Method for uploading image
        public void ImageUploadGreaterThan1MB(string UploadedImage)
        {

            //Click Others
            Others.Click();

            //Image Uploading Greater Than 1MB  
            ImageUpload.SendKeys(UploadedImage);
        }

        //Error Message For Uploading Image Greater Than 1MB
        [FindsBy(How = How.CssSelector, Using = "div.toast-message")]
        private IWebElement ImageUploadClick { get; set; }


       public string ErrorMessageImageUpload()
        {
            return ImageUploadClick.Text.ToString();
            
        }


        //TC/Personal/20	Verify that error message is displayed for dulplicated "National Id"
        //Author : Susmita Kar


       // For National Id Text

       [FindsBy(How = How.Name, Using = "txtNId")]
       private IWebElement NationalIdText { get; set; }

       public void NationalIdInput(string NationalId)
       {

           //Click Others
            Others.Click();
           //Enter National Id  
            TestActions.typeText(NationalIdText, NationalId);
       }


       //TC/ContactInfo/001 Verify that error message is displayed for mandatory field "Present Address"
       //Author : Susmita Kar

       public void EmployeeSetupFormInputWithoutPresentAddress(string Name, string Religion, string DateOfBirth, string PresentAddress, string PermanentAddress, string Company, string Department, string Section, string Designation, string AttendenceGroup, string JoiningDate)
       {
           //Input Personal Tab Mendatory Field
           TestActions.typeText(NameEnter, Name);
           TestActions.selectByVisibleText(ReligionSelect, Religion);
           TestActions.typeText(DateOfBirthEnter, DateOfBirth);
            
           //Click Contact Information
           ContactInfo.Click();
          
           //Input Contact Info Tab Mendatory Field Without Present Address
           TestActions.typeText(PermanentAddressEnter, PermanentAddress);
           
           //Click Job Details
           JobDetails.Click();

           //Input Job Details Tab Mendatory Field
           TestActions.selectByVisibleText(CompanySelect, Company);
           TestActions.selectByVisibleText(DepartmentSelect, Department);
           TestActions.selectByVisibleText(SectionSelect, Section);
           TestActions.selectByVisibleText(DesignationSelect, Designation);
           TestActions.selectByVisibleText(AttendanceGroupSelect, AttendenceGroup);
           TestActions.typeText(JoiningDateSelect, JoiningDate);
           SaveButton.Click();

       }



        //TC/ContactInfo/004 Verify that error message is displayed for mandatory field "Permanant Address"
        //Author : Susmita Kar

       public void EmployeeSetupFormInputWithoutPermanentAddress(string Name, string Religion, string DateOfBirth, string PresentAddress, string PermanentAddress, string Company, string Department, string Section, string Designation, string AttendenceGroup, string JoiningDate)
       {
           //Input Personal Tab Mendatory Field
           TestActions.typeText(NameEnter, Name);
           TestActions.selectByVisibleText(ReligionSelect, Religion);
           TestActions.typeText(DateOfBirthEnter, DateOfBirth);

           //Click Contact Information
           ContactInfo.Click();

           //Input Contact Info Tab Mendatory Field Without Permanent Address
           TestActions.typeText(PresentAddressEnter, PresentAddress);
           

           //Click Job Details
           JobDetails.Click();

           //Input Job Details Tab Mendatory Field
           TestActions.selectByVisibleText(CompanySelect, Company);
           TestActions.selectByVisibleText(DepartmentSelect, Department);
           TestActions.selectByVisibleText(SectionSelect, Section);
           TestActions.selectByVisibleText(DesignationSelect, Designation);
           TestActions.selectByVisibleText(AttendanceGroupSelect, AttendenceGroup);
           TestActions.typeText(JoiningDateSelect, JoiningDate);
           SaveButton.Click();

       }

        //For E-Mail Field

           [FindsBy(How = How.Name, Using = "txtEmail")]
           private IWebElement EmailAddressEnter { get; set; }

           
        //Error message for invalid E-Mail

           [FindsBy(How = How.CssSelector, Using = "div.tooltip-inner")]
           private IWebElement ErrorMessageEmail { get; set; }

       //Method For Displaying Error Message for Invalid E-Mail 
       public string ErrorMessageForEnteringInvalidEmail()
        {
            return ErrorMessageEmail.Text.ToString();
            
        }


       //E-Mail Click
       [FindsBy(How = How.XPath, Using = "//div[@id='tab_1_2']/div[3]/div[2]/div/label")]
       private IWebElement Email { get; set; }

       //TC/ContactInfo/010 Verify that error message is displayed for invalid email address
       //Author : Susmita Kar

       public void InvalidEmail(string EmailAddress)
       {
           
           //Click Contact Information
           ContactInfo.Click();

           //Input invalid E-Mail
           TestActions.typeText(EmailAddressEnter, EmailAddress);

           //Click Contact Information
            Email.Click();


         
       }



       // //for Business Unit in the combo

       //[FindsBy(How = How.XPath, Using = "//div/div[3]/div/div[2]/div/select/option[2]")]
       //private IWebElement BusinessUnitCombo { get; set; }

       ////Method For Displaying Business Unit in the combo 
       //public string DisplayBusinessUnitNameInCombo()
       // {
       //     return BusinessUnitCombo.Text.ToString();

       // }


        //TC/JobDetails/004 Verify that clicking on "Business Unit" is displayed list of Business Unit name according to the company
        //Author : Susmita Kar


       public void ListOfBusinessUnit(string Company, string BusinessUnit)
       {


           //Click Job Details
           JobDetails.Click();

           //Select Company
           TestActions.selectByVisibleText(CompanySelect, Company);

           //Select Business Unit
           TestActions.selectByVisibleText(BusinessUnitSelect, BusinessUnit);

           
       }


      //
        [FindsBy(How = How.CssSelector, Using = "th.datepicker-switch")]
       private IWebElement ErrorMessageDateFormatText { get; set; }


    
       //Method For Displaying Business Unit in the combo 
           public string ErrorMessageDateFormat()
            {
                return ErrorMessageDateFormatText.Text.ToString();

            }



        //TC/JobDetails/026 Verify that error message is displayed if date format (dd/MM/YYYY) is wrong 
        //Author : Susmita Kar

       public void DateFormat(string DateOfBirth)
       {
          
           //Enter Date of Birth
           TestActions.typeText(DateOfBirthEnter, DateOfBirth);

          
         
       }



        //TC/Weekly Holiday/05 Verify that user can remove holiday from "Weekly Holiday" field by clicking (x) sign besides holiday name
        //Author : Susmita Kar


       public void RemoveHoliday()
       {

           //Remove Holiday  clicking (x) sign besides holiday name
           //typeText(DateOfBirthEnter, DateOfBirth);



       }


       //TC/Weekly Holiday/002  Verify that error message is displayed for mandatory field "weekly holiday"
       //Author : Susmita Kar


       public void EmployeeSetupFormInputWithoutWeeklyHoliday(string Name, string Religion, string DateOfBirth, string PresentAddress, string PermanentAddress, string Company, string Department, string Section, string Designation, string AttendenceGroup, string JoiningDate, string Friday)
       {
           //Input Personal Tab Mendatory Field
           TestActions.typeText(NameEnter, Name);
           TestActions.selectByVisibleText(ReligionSelect, Religion);
           TestActions.typeText(DateOfBirthEnter, DateOfBirth);

           //Click Contact Information
           ContactInfo.Click();

           //Input Contact Info Tab Mendatory Field
           TestActions.typeText(PermanentAddressEnter, PermanentAddress);
           TestActions.typeText(PresentAddressEnter, PresentAddress);

           //Click Job Details
           JobDetails.Click();

           //Input Job Details Tab Mendatory Field
           TestActions.selectByVisibleText(CompanySelect, Company);
           TestActions.selectByVisibleText(CompanySelect, Company);
           TestActions.selectByVisibleText(CompanySelect, Company);
           TestActions.selectByVisibleText(DepartmentSelect, Department);
           TestActions.selectByVisibleText(SectionSelect, Section);
           TestActions.selectByVisibleText(DesignationSelect, Designation);
           TestActions.selectByVisibleText(AttendanceGroupSelect, AttendenceGroup);
           TestActions.typeText(JoiningDateSelect, JoiningDate);

           //Remove Weekly Holiday Friday
           WeeklyHolidayRemoveClick.Click();
           Thread.Sleep(5000);

           //Click Save Button
           SaveButton.Click();

       }

       //for Weekly holiday error message

       [FindsBy(How = How.XPath, Using = "//div[4]/div/div/div[2]/div[2]")]
       private IWebElement ErrorMessageWeeklyHolidayText { get; set; }


       //Method for displaying error message for Weekly Holiday
       public string ErrorMessageForMendatoryFieldWeeklyHoliday()
       {
           return ErrorMessageWeeklyHolidayText.Text.ToString();

       }



       //TC/Weekly Holiday/003  Verify that user can set multiple "holiday" on "Weekly Holiday" field
       //Author : Susmita Kar

       public void SelectMultipleHoliday(string Saturday, string Sunday)
       {

           //Select Saturday
           TestActions.selectByVisibleText(SaturdaySelect, Saturday);
           //Select Sunday
           TestActions.selectByVisibleText(SundaySelect, Sunday);

       }

       //TC/Weekly Holiday/001  Verify that "Weekly Holiday" name is not duplicate 
       //Author : Susmita Kar


       public void SelectMultipleHolidayForCheckingDuplicateValue(string Saturday, string Sunday)
       {

           //Select Saturday
           TestActions.selectByVisibleText(SaturdaySelect, Saturday);
           //Select Sunday
           TestActions.selectByVisibleText(SundaySelect, Sunday);


       }







       //TC/Weekly Holiday/05 Verify that user can remove holiday from "Weekly Holiday" field by clicking (x) sign besides holiday name
       //Author : Susmita Kar

       //for Weekly Holiday Friday
       [FindsBy(How = How.XPath, Using = "//div/div/div/ul/li/div")]
       private IWebElement FridayRemoveClick { get; set; }

       //for Weekly Holiday Saturday
       [FindsBy(How = How.XPath, Using = "//li[2]/input")]
       private IWebElement SaturdaySelect { get; set; }

       public string ReturnWeeklyHolidaySaturday()
       {
           return SaturdaySelect.Text.ToString();
       }


       //for Weekly Holiday Sunday
       [FindsBy(How = How.XPath, Using = "///div[4]/div/div/div/ul")]
       private IWebElement SundaySelect { get; set; }

              

       public string ReturnWeeklyHolidaySunday()
       {
           return SundaySelect.Text.ToString();
       }



       //for Weekly Holiday Remove Click
       [FindsBy(How = How.XPath, Using = "//div[4]/div/div/div/ul/li/a")]
       private IWebElement WeeklyHolidayRemoveClick { get; set; }

       public void RemoveWeeklyHoliday(string Friday, string Saturday, string Sunday)
       {


           //Click Job Details
            JobDetails.Click();

           ////Select Saturday
            TestActions.selectByVisibleText(SaturdaySelect, Saturday);

           ////Select Sunday
          // TestActions.selectByIndex(SundaySelect,1);

           ////Remove Weekly Holiday Friday
           //WeeklyHolidayRemoveClick.Click();

           ////Remove Weekly Holiday Saturday
          // WeeklyHolidayRemoveClick.Click();

           ////Remove Weekly Holiday Sunday
           //WeeklyHolidayRemoveClick.Click();

       }



       //TC/SearchEmployee/001 Verify that user can search an employee by using single or multiple Employee IDs
       //Author : Susmita Kar

       //For Employee IDs
       [FindsBy(How = How.Id, Using = "Text1")]
       private IWebElement EmployeeIdEnter { get; set; }

        //For Search Option
       [FindsBy(How = How.Id, Using = "Button1")]
       private IWebElement SearchClick { get; set; }


        //Method for Search Employee By Id
       public void SearchEmployeeId(string Id)
       {

           //Enter Employee IDs
           TestActions.typeText(EmployeeIdEnter, Id);
           
           //Click Search Button
           SearchClick.Click();
           


       }

        //For verifying Employee Id in the grid

       [FindsBy(How = How.XPath, Using = "//td[3]")]
       private IWebElement MessegeEmployeeId { get; set; }


       //Method For Verify Employee Id  in the grid

       public string ReturnSearchEmployeeID()
       {
           return MessegeEmployeeId.Text.ToString();
       }


       //TC/SearchEmployee/002 Verify that user can search an employee by using Employee Name
       //Author : Susmita Kar
      
       //For Employee Name
       [FindsBy(How = How.Id, Using = "Text3")]
       private IWebElement EmployeeNameEnter { get; set; }

       //Method for Search Employee By Name
       public void SearchEmployeeName(string Name)
       {

           //Enter Employee Name
           TestActions.typeText(EmployeeNameEnter, Name);

           //Click Search Button
           SearchClick.Click();



       }

       //For verifying Employee Name in the grid

       [FindsBy(How = How.XPath, Using = "//td[4]")]
       private IWebElement MessegeEmployeeName { get; set; }


       //Method For Verify Employee Name in the grid

       public string ReturnSearchEmployeeName()
       {
           return MessegeEmployeeName.Text.ToString();
       }

        //TC/SearchEmployee/005 Verify that user can search an employee by using Employee ID and Name
        //Author : Susmita Kar

       public void SearchEmployeeIdName(string Id, string Name)
       {

           //Enter Employee IDs
           TestActions.typeText(EmployeeIdEnter, Id);

           //Enter Employee Name
           TestActions.typeText(EmployeeNameEnter, Name);

           //Click Search Button
           SearchClick.Click();



       }


        //TC/SearchEmployee/009 Verify that clicking on ">" (Next) button will display employee information from Next page of the employe list
        //Author : Susmita Kar


        //For Creation Date From
         [FindsBy(How = How.Id, Using = "Text5")]
       private IWebElement CreationDateFromSelect { get; set; }


         //For Creation Date To                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
         [FindsBy(How = How.Id, Using = "Text6")]
         private IWebElement CreationDateToSelect { get; set; }

        //For Button 2
         [FindsBy(How = How.XPath, Using = "//div[2]/div/ul/li[3]/a")]
         private IWebElement Button2Select { get; set; }

        //Method For Verifying Button 2 Click
         public string ReturnButton2Click()
         {
             return Button2Select.Text.ToString();
         }

        //For Calender Previous Button
         [FindsBy(How = How.CssSelector, Using = "th.prev")]
         private IWebElement CalenderPreviousButtonClick { get; set; }





       public void SearchEmployeeCreationDate(string FromDate, string ToDate)
       {

           ////Click "<" in the calender
            //CalenderPreviousButtonClick.Click();
            //CalenderPreviousButtonClick.Click();

           //Enter Creation Date From
           TestActions.typeText(CreationDateFromSelect, FromDate);
           Thread.Sleep(8000);

           //Enter Creation Date To
           TestActions.typeText(CreationDateToSelect, ToDate);

           //Click Search Button
           SearchClick.Click();

           //Click Button 2
           Button2Select.Click();


       }


        //TC/SearchEmployee/012 Verify that employee information can be edited
        //Author : Susmita Kar

        //For Edit Option
       [FindsBy(How = How.XPath, Using = "//td[2]/img")]
       private IWebElement EditOptionClick { get; set; }

        //For Editing Spouse Name
       [FindsBy(How = How.XPath, Using = "//div[4]/div[2]/div/input")]
       private IWebElement EditSpouseName { get; set; }

       //For Clicking No button in the Pop Up Message

       [FindsBy(How = How.XPath, Using = "//div[3]/button[2]")]
       private IWebElement NoButtonClick { get; set; }

       
        //Method for editing information
        public void EditingEmployeeInformation(string Id,string SpouseName)
       {

           //Enter Employee IDs
           TestActions.typeText(EmployeeIdEnter, Id);

           //Click Search Button
           SearchClick.Click();

           //Click Edit Option
           EditOptionClick.Click();

           Thread.Sleep(3000);

            //Edit Spouse Name
           TestActions.typeText(EditSpouseName, SpouseName);

           //Click Save
           SaveButton.Click();

            //Click No in the Pop Up Message
             NoButtonClick.Click();
           
            //Enter Employee IDs
           TestActions.typeText(EmployeeIdEnter, Id);
          
            //Click Search Button
           SearchClick.Click();

           //Click Edit Option
           EditOptionClick.Click();

       }

        //Method For Verify Employee Name in the grid

        public string ReturnEditedInformation()
        {
            return EditSpouseName.GetAttribute("value");
        }



        //TC/SearchEmployee/007 Verify that selecting the  record number from record per page drop down list will display number of records at the employee list
        //Author : Susmita Kar

        //for "Record Per Page" combo select
        [FindsBy(How = How.XPath, Using = "//label/select")]
        private IWebElement RecordPerPageSelect { get; set; }


        //For first employee code from the grid list

        [FindsBy(How = How.XPath, Using = "//td[3]")]
        private IWebElement ReturnFirstEmployeeCodeFromList { get; set; }

        


        //For second employee code from the grid list

        [FindsBy(How = How.XPath, Using = "//tr[15]/td[3]")]
        private IWebElement ReturnSecondEmployeeCodeFromList { get; set; }



        public void DisplayEmployeeList(string FromDate, string ToDate,string RecordNumber)
        {

            ////Click "<" in the calender
            //CalenderPreviousButtonClick.Click();
            //CalenderPreviousButtonClick.Click();

            //Enter Creation Date From
            TestActions.typeText(CreationDateFromSelect, FromDate);

            //Enter Creation Date To
            TestActions.typeText(CreationDateToSelect, ToDate);

            //Click Search Button
            SearchClick.Click();

           //Select number from "Record Per Page" Combo
            TestActions.selectByVisibleText(RecordPerPageSelect, RecordNumber);

        }


        //TC/SearchEmployee/008 Verify that Employee list can be sorted by column name
        //Author : Susmita Kar


        //For First Employee Name in the grid
         
        [FindsBy(How=How.XPath, Using = "//td[4]")]
         private IWebElement FirstEmployeeNameText { get;set;}

       
        //Method for returing First Employee Name from the grid
          public string ReturnFirstEmployeeName()
        {
            return FirstEmployeeNameText.Text.ToString();
        }


        //For Second Employee Name in the grid
         
        [FindsBy(How=How.XPath, Using = "//tr[2]/td[4]")]
         private IWebElement SecondEmployeeNameText { get;set;}

        
        //Method for returing Second Employee Name from the grid
          public string ReturnSecondEmployeeName()
        {
            return SecondEmployeeNameText.Text.ToString();
        }

         //For Third Employee Name in the grid
         
        [FindsBy(How=How.XPath, Using = "//tr[3]/td[4]")]
         private IWebElement ThirdEmployeeNameText { get;set;}

        
        //Method for returing Third Employee Name from the grid
          public string ReturnThirdEmployeeName()
        {
           return ThirdEmployeeNameText.Text.ToString();
        }




        //For Accending Button

          [FindsBy(How = How.XPath, Using = "//table[@id='tblGrid']/thead/tr/th[4]")]
          private IWebElement AccendingButtonClick { get; set; }

       
        //Method For Sorting Employee By Name in the grid
        public void SortingEmployeeByName(string FromDate, string ToDate)
        {

            //Enter Creation Date From
            TestActions.typeText(CreationDateFromSelect, FromDate);

            //Enter Creation Date To
            TestActions.typeText(CreationDateToSelect, ToDate);

            //Click Search Button
            SearchClick.Click();

            //Click Accending Button
            AccendingButtonClick.Click();



        }

      
    }


 }

       


      


  



