﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using OpenQA.Selenium;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.PageObjects;


namespace HRMSUIAutomation.Page
{
    class DashboardPage
    {
        private IWebDriver driver;

        //CommonAction commonAction=new CommonAction();
      
    
         public DashboardPage(IWebDriver driver)
        {
		    this.driver = driver;
	    }

         //Dashboard
         [FindsBy(How = How.XPath, Using = "//form[@id='form1']/div[5]/div[2]/div/div/div/h3")]
         private IWebElement DashboardTitle { get; set; }

         public string DashboardTitleText()
         {
             string dashboardTitle = DashboardTitle.Text.ToString();
             return dashboardTitle;
         }

        // General Configuration Menu
         [FindsBy(How = How.XPath, Using = "//li[4]/a/span")]
         private IWebElement GeneralConfigurationMenu { get; set; }
         // Group of Company SubMenu
         [FindsBy(How = How.LinkText, Using = "Group of Company")]
         private IWebElement GroupOfCompanySubMenu { get; set; }
         // Company SubMenu
         [FindsBy(How = How.LinkText, Using = "Company")]
         private IWebElement CompanySubMenu { get; set; }
         // Business Unit / Branch SubMenu
         [FindsBy(How = How.LinkText, Using = "Business Unit / Branch")]
         private IWebElement BusinessUnitSubMenu { get; set; }
         // Department SubMenu
         [FindsBy(How = How.LinkText, Using = "Department")]
         private IWebElement DepartmentSubMenu { get; set; }
         // Section SubMenu
         [FindsBy(How = How.LinkText, Using = "Section")]
         private IWebElement SectionSubMenu { get; set; }
         // Production Unit SubMenu
         [FindsBy(How = How.LinkText, Using = "Production Unit")]
         private IWebElement ProductionUnitSubMenu { get; set; }
         // Production Line SubMenu
         [FindsBy(How = How.LinkText, Using = "Production Line")]
         private IWebElement ProductionLineSubMenu { get; set; }
         // Designation SubMenu
         [FindsBy(How = How.LinkText, Using = "Designation")]
         private IWebElement DesignationSubMenu { get; set; }
         // Grade Type SubMenu
         [FindsBy(How = How.LinkText, Using = "Grade Type")]
         private IWebElement GradeTypeSubMenu { get; set; }
         // Grade  SubMenu
         [FindsBy(How = How.LinkText, Using = "Grade")]
         private IWebElement GradeSubMenu { get; set; }
         // Grade Designation Map  SubMenu
         [FindsBy(How = How.LinkText, Using = "Grade Designation Map")]
         private IWebElement GradeDesignationMapSubMenu { get; set; }
         // Documents SubMenu
         [FindsBy(How = How.LinkText, Using = "Documents")]
         private IWebElement DocumentsSubMenu { get; set; }
         // Announcement SubMenu
         [FindsBy(How = How.LinkText, Using = "Announcement")]
         private IWebElement AnnouncementSubMenu { get; set; }
         // Meeting SubMenu
         [FindsBy(How = How.LinkText, Using = "Meeting")]
         private IWebElement MeetingSubMenu { get; set; }

         public void NavigateToGroupOfCompanyPage()
         {
             GeneralConfigurationMenu.Click();
             GroupOfCompanySubMenu.Click();
         }
         public void NavigateToCompanyPage()
         {
             GeneralConfigurationMenu.Click();
             CompanySubMenu.Click();
         }
         public void NavigateToBusinessUnitPage()
         {
             GeneralConfigurationMenu.Click();
             BusinessUnitSubMenu.Click();
         }
         public void NavigateToDepartmentPage()
         {
             GeneralConfigurationMenu.Click();
             DepartmentSubMenu.Click();
         }
         public void NavigateToSectionPage()
         {
             GeneralConfigurationMenu.Click();
             SectionSubMenu.Click();
         }
         public void NavigateToProductionUnitPage()
         {
             GeneralConfigurationMenu.Click();
             ProductionUnitSubMenu.Click();
         }
         public void NavigateToProductionLinePage()
         {
             GeneralConfigurationMenu.Click();
             ProductionLineSubMenu.Click();
         }
         public void NavigateToDesignationPage()
         {
             GeneralConfigurationMenu.Click();
             DesignationSubMenu.Click();
         }
         public void NavigateToGradeTypePage()
         {
             GeneralConfigurationMenu.Click();
             GradeTypeSubMenu.Click();
         }
         public void NavigateToGradePage()
         {
             GeneralConfigurationMenu.Click();
             GradeSubMenu.Click();
         }
         public void NavigateToGradeDesignationMapPage()
         {
             GeneralConfigurationMenu.Click();
             GradeDesignationMapSubMenu.Click();
         }
         public void NavigateToDocumentsPage()
         {
             GeneralConfigurationMenu.Click();
             DocumentsSubMenu.Click();
         }
         public void NavigateToAnnouncementPage()
         {
             GeneralConfigurationMenu.Click();
             AnnouncementSubMenu.Click();
         }
         public void NavigateToMeetingPage()
         {
             GeneralConfigurationMenu.Click();
             MeetingSubMenu.Click();
         }




        
        // Employe Management Menu
        [FindsBy(How = How.XPath, Using = "//li[5]/a/span")]
         private IWebElement EmpManagementMenu{ get; set; }

        //Employees SubMenu CLick
        [FindsBy(How = How.XPath, Using = "//li[5]/ul/li[3]/a/span")]
        private IWebElement EmployeesSubMenu { get; set; }

        
        //Employee Information Click
        [FindsBy(How = How.XPath, Using = "//li[5]/ul/li[3]/ul/li[3]/a")]
        private IWebElement EmployeeInformation { get; set; }

        //Employee Requisition Click from left
        [FindsBy(How = How.LinkText, Using = "Requisition")]
        private IWebElement EmployeeRequisition { get; set; }

        //Employee Selection Click from left
        [FindsBy(How = How.LinkText, Using = "Selection")]
        private IWebElement EmployeeSelection { get; set; }



        public void NavigateToEmpSetupForm()
        {
            EmpManagementMenu.Click();
            EmployeesSubMenu.Click();
            EmployeeInformation.Click();

        }

        public void NavigateToEmployeeRequisitionForm()
        {
            EmpManagementMenu.Click();
            EmployeesSubMenu.Click();
            EmployeeRequisition.Click();

        }

        public void NavigateToEmployeeSelectionForm()
        {
            EmpManagementMenu.Click();
            EmployeesSubMenu.Click();
            EmployeeSelection.Click();

        }


       
    }
}
