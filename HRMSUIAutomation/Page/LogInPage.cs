﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using OpenQA.Selenium;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.PageObjects;


namespace HRMSUIAutomation.Page
{
    class LogInPage
    {

        private IWebDriver driver;

        //CommonAction commonAction=new CommonAction();

        //User Name
        [FindsBy(How = How.Id, Using = "txtUsername")]
        private IWebElement UserName {get;set;}


        //Password
        [FindsBy(How = How.Id, Using = "txtPassword")]
        private IWebElement PassWord { get; set; }
  

        //Log in button
        [FindsBy(How = How.Id, Using = "btnLogin")]
        private IWebElement LogInButton { get; set; }


       




        public LogInPage(IWebDriver driver)
        {
		    this.driver = driver;
	    }

        public void ValidLogIn(string Username, string Password)
        {
            //Enter username 
            UserName.SendKeys(Username);

            //Enter pasword
            PassWord.SendKeys(Password);

            //click on Log in button
            LogInButton.Click();
            
        }
        
       
    }
}
