﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using OpenQA.Selenium;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.PageObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using HRMSUIAutomation.Utilities;

namespace HRMSUIAutomation.Page
{
    [TestClass]
    public class BusinessUnitPage 
    {
     
        private IWebDriver driver;

        public BusinessUnitPage(IWebDriver driver)
        {
		    this.driver = driver;
	    }
        // Find  Company
        [FindsBy(How = How.Id, Using = "ddlCompany")]
        private IWebElement CompanyPosition { get; set; }
        // Find  Name(English)
        [FindsBy(How = How.Id, Using = "txtNameEng")]
        private IWebElement NameEnglishPosition { get; set; }
        // Find  Name(Native)
        [FindsBy(How = How.Id, Using = "txtNameBng")]
        private IWebElement NameNativePosition { get; set; }
        // Find  Address(English)
        [FindsBy(How = How.Id, Using = "txtAddress")]
        private IWebElement AddressEnglishPosition { get; set; }
        // Find  Address(Native)
        [FindsBy(How = How.Id, Using = "txtAddBng")]
        private IWebElement AddressNativePosition { get; set; }
        // Find  Phone No
        [FindsBy(How = How.Name, Using = "txtPhone")]
        private IWebElement PhoneNoPosition { get; set; }
        // Find  Mobile
        [FindsBy(How = How.Name, Using = "txtMobile")]
        private IWebElement MobilePosition { get; set; }
        // Find  Fax
        [FindsBy(How = How.Name, Using = "txtFax")]
        private IWebElement FaxPosition { get; set; }
        // Find  Email
        [FindsBy(How = How.Name, Using = "txtEmail")]
        private IWebElement EmailPosition { get; set; }
        // Find  Web
        [FindsBy(How = How.Name, Using = "txtWeb")]
        private IWebElement WebPosition { get; set; }
        // Find  Remarks
        [FindsBy(How = How.Id, Using = "txtRemarks")]
        private IWebElement RemarksPosition { get; set; }
        // Find  Save Button
        [FindsBy(How = How.Id, Using = "Button1")]
        private IWebElement SaveButton { get; set; }
        // Find  Success message
        [FindsBy(How = How.XPath, Using = "//body/div/div/div[2]")]
        private IWebElement SuccessMessagePosition { get; set; }

         //TC/company/001    Verify that "Code" is auto generated from database after saving information
        //Author : TareQ
        public void AllInputFieldsforBusinessUnitSetup(string Company, string NameEnglish, string NameNative, string AddressEnglish, string AddressNative, string PhoneNo, string Mobile, string Fax, string Email, string Web, string Remarks)
        {
            //Enter Group of Company
            TestActions.selectByVisibleText(CompanyPosition, Company);
            //Enter Name English
            TestActions.typeText(NameEnglishPosition, NameEnglish);
            //Enter Name Native
            TestActions.typeText(NameNativePosition, NameNative);
            //Enter Address English
            TestActions.typeText(AddressEnglishPosition, AddressEnglish);
            //Enter Address Native
            TestActions.typeText(AddressNativePosition, AddressNative);
            //Enter Phone No
            TestActions.typeText(PhoneNoPosition, PhoneNo);
            //Enter Mobile
            TestActions.typeText(MobilePosition, Mobile);
            //Enter Fax
            TestActions.typeText(FaxPosition, Fax);
            //Enter Email
            TestActions.typeText(EmailPosition, Email);
            //Enter Web
            TestActions.typeText(WebPosition, Web);
            //Enter Remarks
            TestActions.typeText(RemarksPosition, Remarks);
            //save button click
            SaveButton.Click();
        }
        public string CheckSuccessMessage()
        {
            string SuccessMessage = SuccessMessagePosition.Text.ToString();
            return SuccessMessage;
        }
    }
}
