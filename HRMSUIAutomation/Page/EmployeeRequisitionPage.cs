﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using OpenQA.Selenium;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.PageObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using HRMSUIAutomation.Utilities;


namespace HRMSUIAutomation.Page
{
    [TestClass]
    public class EmployeeRequisitionPage 
    {

        private IWebDriver driver;

        public EmployeeRequisitionPage(IWebDriver driver)
        {
		    this.driver = driver;
	    }

        //TC/EmplRequisitionList/22 	Verify that employee requisition can do with the available field
        //Author : TareQ

        // Designation Field  
        [FindsBy(How = How.Id, Using = "ddlDesig")]
        private IWebElement DesignationPosition { get; set; }

        //Required Employee Position 
        [FindsBy(How = How.Id, Using = "txtEmpId")]
        private IWebElement RequiredEmployee { get; set; }

        //  Add Button  
        [FindsBy(How = How.Id, Using = "Button1")]
        private IWebElement AddButton { get; set; }

        //  Created Designation  
        [FindsBy(How = How.XPath, Using = "//div/div/div/div/div/div/a")]
        private IWebElement createdDesignationPosition { get; set; }

        // Find position for Company
        [FindsBy(How = How.Id, Using = "ddlCompany1")]
        private IWebElement CompanyPosition { get; set; }
        // Find position for BusinessUnit
        [FindsBy(How = How.Id, Using = "ddlBusinessUnit1")]
        private IWebElement BusinessUnitPosition { get; set; }
        // Find position for Department
        [FindsBy(How = How.Id, Using = "ddlDepartment1")]
        private IWebElement DepartmentPosition { get; set; }
        // Find position for Section
        [FindsBy(How = How.Id, Using = "ddlSection1")]
        private IWebElement SectionPosition { get; set; }
        // Find position for Job Responsibility
        [FindsBy(How = How.Id, Using = "txtJobRespons1")]
        private IWebElement JobResponsibilityPosition { get; set; }
        // Find position for Job Nature
        [FindsBy(How = How.Id, Using = "ddljobNature1")]
        private IWebElement JobNaturePosition { get; set; }
        // Find position for Qualification
        [FindsBy(How = How.Id, Using = "txtQualif1")]
        private IWebElement QualificationPosition { get; set; }
        // Find position for Experience
        [FindsBy(How = How.Id, Using = "txtexperience1")]
        private IWebElement ExperiencePosition { get; set; }
        // Find position for Additional Requirement
        [FindsBy(How = How.Id, Using = "txtAddRequire1")]
        private IWebElement AdditionalRequirementPosition { get; set; }
        // Find position for Additional Requirement
        [FindsBy(How = How.Id, Using = "txtAddRequire1")]
        //private IWebElement AdditionalRequirementPosition { get; set; }
        // Find position for Salary Range
        [FindsBy(How = How.Id, Using = "txtSalary1")]
        private IWebElement SalaryRangePosition { get; set; }
        // Find position for Other Benefits
        [FindsBy(How = How.Id, Using = "txtBenefits1")]
        private IWebElement OtherBenefitsPosition { get; set; }
        // Find position for Job Location
        [FindsBy(How = How.Id, Using = "txtJobLocation1")]
        private IWebElement JobLocationPosition { get; set; }
        // Find position for Gender
        [FindsBy(How = How.Id, Using = "ddlGender1")]
        private IWebElement GenderPosition { get; set; }



        // Find position for Joining Date
        [FindsBy(How = How.Id, Using = "txtSalary1")]
        private IWebElement JoiningDatePosition { get; set; }

        // Find position for Reference
        [FindsBy(How = How.Id, Using = "txtRefference")]
        private IWebElement ReferencePosition { get; set; }
        // Find Note 
        [FindsBy(How = How.XPath, Using = "txtNote")]
        private IWebElement NotePosition { get; set; }

        // Find save position
        [FindsBy(How = How.Id, Using = "btnSave")]
        private IWebElement SaveButton { get; set; }

        // Find position for Yes I Am
        [FindsBy(How = How.XPath, Using = "//div[3]/button")]
        private IWebElement YesIAmButton { get; set; }
        

        public void EmployeeRequisition(string EmployeeDesignation, int NumberOfEmployee, string Company, string Reference,string JoiningDate)
        {
            //Select Designation1
            TestActions.selectByVisibleText(DesignationPosition, EmployeeDesignation);

            //Enter number of Employee
            RequiredEmployee.SendKeys(NumberOfEmployee.ToString());

            //Click on Add button
            AddButton.Click();

            //Click on Created Designation
            createdDesignationPosition.Click();

            //Select Company
            TestActions.selectByVisibleText(CompanyPosition, Company);

            //Select joining date
            TestActions.typeText(JoiningDatePosition, JoiningDate);

            //Select Reference
            TestActions.typeText(ReferencePosition, Reference);

            //Click on Save button
            SaveButton.Click();

            //Click on yes I Am button
            //YesIAmButton.Click();
        }

        //  TC/EmplRequisitionList/06 	Verify that multiple requisition can be applied by using "Designation" and "Required Employee"
        //  Author : TareQ

        // Find position second Created Designation : Admin Asst.
        [FindsBy(How = How.XPath, Using = "//div[2]/div/a")]
        private IWebElement CreatedDesignationSecondPosition { get; set; }
        // Find position for Second Company
        [FindsBy(How = How.Id, Using = "ddlCompany2")]
        private IWebElement SecondCompanyPosition { get; set; }

        // Find position for Second Joining Date
        [FindsBy(How = How.Id, Using = "txtJoiningDate2")]
        private IWebElement SecondJoiningDatePosition { get; set; }

        public void FirstEmployeeRequisition(string EmployeeDesignation, int NumberOfEmployee, string Company, string JoiningDate)
        {
            //Select Designation1
            TestActions.selectByVisibleText(DesignationPosition, EmployeeDesignation);

            //Enter number of Employee
            RequiredEmployee.SendKeys(NumberOfEmployee.ToString());

            //Click on Add button
            AddButton.Click();

            //Click on Created Designation
            createdDesignationPosition.Click();

            //Select Company
            TestActions.selectByVisibleText(CompanyPosition, Company);

            //Select joining date
            TestActions.typeText(JoiningDatePosition, JoiningDate);
        }

        public void SecondEmployeeRequisition(string EmployeeDesignation, int NumberOfEmployee, string Company, string Reference, string JoiningDate)
        {
            //Select Designation1
            TestActions.selectByVisibleText(DesignationPosition, EmployeeDesignation);

            //Enter number of Employee
            RequiredEmployee.SendKeys(NumberOfEmployee.ToString());

            //Click on Add button
            AddButton.Click();

            //Click on Created Designation
            CreatedDesignationSecondPosition.Click();

            //Select Company for second designation
            TestActions.selectByVisibleText(SecondCompanyPosition, Company);

            //Select joining date for second designation
            TestActions.typeText(SecondJoiningDatePosition, JoiningDate);

            //Select Reference
            TestActions.typeText(ReferencePosition, Reference);

            //Click on Save button
            SaveButton.Click();

            //Click on yes I Am button
            //YesIAmButton.Click();
        }



        //TC/EmplRequisitionApplication/03	Verify that "Version" is changed after saving an edited form
        //Author : TareQ
        //Find Code position
        [FindsBy(How = How.Id, Using = "txtCode")]
        private IWebElement CodePosition { get; set; }

        public void CodeInput(string Code)
        {
            
            // Enter Click
            CodePosition.SendKeys(Keys.Enter);
            //save button click
            SaveButton.Click();
            //Yes I Am click
            YesIAmButton.Click();
        }


        //TC/EmplRequisitionApplication/09	Verify that clicking on "Clear" button discard the operation and refresh the form
        //Author : TareQ

        // Find clear position
        [FindsBy(How = How.XPath, Using = "//button[3]")]
        private IWebElement ClearButton { get; set; }

        public void CodeInputForClear(string Code)
        {
            ///Select Code
            TestActions.typeText(CodePosition, Code);
            //Keyboard Enter Click
            CodePosition.SendKeys(Keys.Enter);
            //clear button click
            ClearButton.Click();
        }

        //TC//EmplRequisitionList/01	Verify that error message is displayed for mandatory field "Designation"
        //Author : TareQ

        // Find Error Message position
        [FindsBy(How = How.XPath, Using = "//div[2]/div/div/div/div/div[2]")]
        private IWebElement ErrorMessagePositionForDesignation { get; set; }
        public void MandatoryFieldsToTestDesignation(int NumberOfEmployee)
        {
            //Enter number of Employee
            RequiredEmployee.SendKeys(NumberOfEmployee.ToString());
            // Click Add
            AddButton.Click();     
        }

        public string ErrorMessageForMandatoryFieldDesignation()
        {
            string errorMessage = ErrorMessagePositionForDesignation.Text.ToString();
            return errorMessage;
        }



        //TC//EmplRequisitionList/03	Verify that error message is displayed for mandatory field "Required Employee"
        //Author : TareQ


        // Find Error Message position
        [FindsBy(How = How.XPath, Using = "//div[2]/div/div/div/div/div[2]")]
        private IWebElement ErrorMessagePositionForRequiredEmployee { get; set; }
        public void MandatoryFieldsToTestRequiredEmployee(string Designation)
        {
            //Enter Designation
            TestActions.selectByVisibleText(DesignationPosition, Designation);
            // Click Add
            AddButton.Click();
        }
        
        //Method for error message check
        public string ErrorMessageForMandatoryFieldRequiredEmployee()
        {
            string errorMessage = ErrorMessagePositionForRequiredEmployee.Text.ToString();
            return errorMessage;
        }




        //TC//EmplRequisitionList/16	Verify that error message is displayed for mandatory field "Company"
        //Author : TareQ


        // Find Error Message position for company
        [FindsBy(How = How.XPath, Using = "//body/div/div/div[2]")]
        private IWebElement ErrorMessagePositionForCompany { get; set; }


        public void MandatoryFieldsToTestCompany(string Designation, int NumberOfEmployee,String JoiningDate)
        {
            //Enter Designation
            TestActions.selectByVisibleText(DesignationPosition, Designation);
            //Enter number of Employee
            RequiredEmployee.SendKeys(NumberOfEmployee.ToString());
            // Click Add
            AddButton.Click();
            // Click Designation : Admin Officcer
            createdDesignationPosition.Click();
            //Enter Joining Date
            TestActions.typeText(JoiningDatePosition, JoiningDate);
            // Click Save
            SaveButton.Click();
        }

        //Method for error message check
        public string ErrorMessageForMandatoryFieldCompany()
        {
            string errorMessage = ErrorMessagePositionForCompany.Text.ToString();
            return errorMessage;
        }

        //TC//EmplRequisitionList/18	Verify that error message is displayed for mandatory field "Joining Date"
        //Author : TareQ


        // Find Error Message position for Joining Date
        [FindsBy(How = How.XPath, Using = "//body/div/div/div[2]")]
        private IWebElement ErrorMessagePositionForJoiningDate { get; set; }
        public void MandatoryFieldsToTestJoiningDate(string Designation, int NumberOfEmployee, String Company)
        {
            //Enter Designation
            TestActions.selectByVisibleText(DesignationPosition, Designation);
            //Enter number of Employee
            RequiredEmployee.SendKeys(NumberOfEmployee.ToString());
            // Click Add
            AddButton.Click();
            // Click Designation : Admin Officcer
            createdDesignationPosition.Click();
            //Enter Joining Date
            TestActions.selectByVisibleText(CompanyPosition,Company);
            // Click Save
            SaveButton.Click();
        }

        //Method for error message check
        public string ErrorMessageForMandatoryFieldJoiningDate()
        {
            string errorMessage = ErrorMessagePositionForJoiningDate.Text.ToString();
            return errorMessage;
        }

        //TC//EmplRequisitionList/18	Verify that error message is displayed for mandatory field "Reference"
        //Author : TareQ


        // Find Error Message position for Reference
        [FindsBy(How = How.XPath, Using = "//body/div/div/div[2]")]
        private IWebElement ErrorMessagePositionForReference { get; set; }
        public void MandatoryFieldsToTestReference(string Designation, int NumberOfEmployee, String Company,string JoiningDate)
        {
            //Enter Designation
            TestActions.selectByVisibleText(DesignationPosition, Designation);
            //Enter number of Employee
            RequiredEmployee.SendKeys(NumberOfEmployee.ToString());
            // Click Add
            AddButton.Click();
            // Click Designation : Admin Officcer
            createdDesignationPosition.Click();
            //Select Company
            TestActions.selectByVisibleText(CompanyPosition, Company);
            //Select Joining Date
            TestActions.typeText(JoiningDatePosition, JoiningDate);

            // Click Save
            SaveButton.Click();
        }

        //Method for error message check
        public string ErrorMessageForMandatoryFieldReference()
        {
            string errorMessage = ErrorMessagePositionForReference.Text.ToString();
            return errorMessage;
        }
    }
}
