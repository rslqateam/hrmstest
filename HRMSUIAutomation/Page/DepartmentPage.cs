﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using OpenQA.Selenium;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.PageObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using HRMSUIAutomation.Utilities;
namespace HRMSUIAutomation.Page
{
    [TestClass]
    public class DepartmentPage 
    { 
        private IWebDriver driver;

        public DepartmentPage(IWebDriver driver)
        {
		    this.driver = driver;
	    }

        // Find  Company
        [FindsBy(How = How.Id, Using = "ddlCompany")]
        private IWebElement CompanyPosition { get; set; }
        // Find  Name(English)
        [FindsBy(How = How.Id, Using = "txtNameEng")]
        private IWebElement NameEnglishPosition { get; set; }
        // Find  Name(Native)
        [FindsBy(How = How.Id, Using = "txtNameBng")]
        private IWebElement NameNativePosition { get; set; }
        // Find  Remarks
        [FindsBy(How = How.Id, Using = "txtRemarks")]
        private IWebElement RemarksPosition { get; set; }
        // Find  Save Button
        [FindsBy(How = How.Id, Using = "Button1")]
        private IWebElement SaveButton { get; set; }
        // Find  Success message
        [FindsBy(How = How.XPath, Using = "//body/div/div/div[2]")]
        private IWebElement SuccessMessagePosition { get; set; }


        //TC/Department/001    Verify that "Code" is auto generated from database after saving information
        //Author : TareQ

        public void AllInputFieldsforDepartment(string Company, string NameEnglish, string NameNative, string Remarks)
        {
            //Enter Group of Company
            TestActions.selectByVisibleText(CompanyPosition, Company);
            //Enter Name English
            TestActions.typeText(NameEnglishPosition, NameEnglish);
            //Enter Name Native
            TestActions.typeText(NameNativePosition, NameNative);
            //Enter Remarks
            TestActions.typeText(RemarksPosition, Remarks);
            //save button click
            SaveButton.Click();
        }
        public string CheckSuccessMessage()
        {
            string SuccessMessage = SuccessMessagePosition.Text.ToString();
            return SuccessMessage;
        }
    }
}
