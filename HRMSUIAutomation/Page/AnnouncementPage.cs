﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using OpenQA.Selenium;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.PageObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HRMSUIAutomation.Utilities;
namespace HRMSUIAutomation.Page
{
    [TestClass]
    public class AnnouncementPage
    {
        private IWebDriver driver;

        public AnnouncementPage(IWebDriver driver)
        {
		    this.driver = driver;
	    }
        // Find  Grade
        [FindsBy(How = How.Id, Using = "s2id_autogen1")]
        private IWebElement GradePosition { get; set; }
        // Find  Title
        [FindsBy(How = How.Name, Using = "txtTitle")]
        private IWebElement TitlePosition { get; set; }
        // Find  Description
        [FindsBy(How = How.XPath, Using = "//div[2]/div/div/input")]
        private IWebElement DescriptionPosition { get; set; }
        // Find  Additional Information
        [FindsBy(How = How.Name, Using = "txtName")]
        private IWebElement AdditionalInformationPosition { get; set; }
        // Find  Activation Date
        [FindsBy(How = How.XPath, Using = "//div[2]/div[3]/div/input")]
        private IWebElement ActivationDatePosition { get; set; }
        // Find  Document
        [FindsBy(How = How.Id, Using = "fileToUpload")]
        private IWebElement DocumentPosition { get; set; }
        // Find  Save Button
        [FindsBy(How = How.Id, Using = "Button1")]
        private IWebElement SaveButton { get; set; }
        // Find  Success message
        [FindsBy(How = How.XPath, Using = "//body/div/div/div[2]")]
        private IWebElement SuccessMessagePosition { get; set; }

        //TC/Announcement/001    Verify that "Code" is auto generated from database after saving information
        //Author : TareQ

        public void AllInputFieldsforAnnouncement(int IndexOfGrade, string Title, string Description,string AdditionalInformation, string ActivationDate,string Document)
        {
            // Enter Grade
            // TestActions.returnListValueByIndex(GradePosition, IndexOfGrade);
            // Enter Title
            TestActions.typeText(TitlePosition, Title);
            //Enter Description 
            TestActions.typeText(DescriptionPosition, Description);
            //Enter AdditionalInformation 
            TestActions.typeText(AdditionalInformationPosition, Description);
            //Enter Activation Date
            TestActions.typeText(ActivationDatePosition, ActivationDate);
            //Enter Documents
            TestActions.typeText(DocumentPosition, Document);
            //save button click
            SaveButton.Click();
        }
        public string CheckSuccessMessage()
        {
            string SuccessMessage = SuccessMessagePosition.Text.ToString();
            return SuccessMessage;
        }
    }
}
