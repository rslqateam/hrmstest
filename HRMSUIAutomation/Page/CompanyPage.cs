﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using OpenQA.Selenium;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.PageObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;

using HRMSUIAutomation.Utilities;

namespace HRMSUIAutomation.Page
{
   
    public class CompanyPage 
    {
       
        private IWebDriver driver;

        public CompanyPage(IWebDriver driver)
        {
		    this.driver = driver;
	    }
         // Find  Group of Company
        [FindsBy(How = How.Id, Using = "ddlGroupCompany")]
        private IWebElement GroupOfCompanyPosition { get; set; }
         // Find  Name(English)
        [FindsBy(How = How.Id, Using = "txtNameEng")]
        private IWebElement NameEnglishPosition { get; set; }
        // Find  Name(Native)
        [FindsBy(How = How.Id, Using = "txtNameBng")]
        private IWebElement NameNativePosition { get; set; }
        // Find  Address(English)
        [FindsBy(How = How.Id, Using = "txtAddress")]
        private IWebElement AddressEnglishPosition { get; set; }
        // Find  Address(Native)
        [FindsBy(How = How.Id, Using = "txtAddBng")]
        private IWebElement AddressNativePosition { get; set; }
        // Find  Phone No
        [FindsBy(How = How.Name, Using = "txtPhone")]
        private IWebElement PhoneNoPosition { get; set; }
        // Find  Mobile
        [FindsBy(How = How.Name, Using = "txtMobile")]
        private IWebElement MobilePosition { get; set; }
        // Find  Fax
        [FindsBy(How = How.Name, Using = "txtFax")]
        private IWebElement FaxPosition { get; set; }
        // Find  Email
        [FindsBy(How = How.Name, Using = "txtEmail")]
        private IWebElement EmailPosition { get; set; }
        // Find  Web
        [FindsBy(How = How.Name, Using = "txtWeb")]
        private IWebElement WebPosition { get; set; }
        // Find  Remarks
        [FindsBy(How = How.Id, Using = "txtRemarks")]
        private IWebElement RemarksPosition { get; set; }
        // Find  Logo Upload 
        [FindsBy(How = How.Id, Using = "Img1")]
        private IWebElement LogoPosition { get; set; }
        // Find  status Active
        [FindsBy(How = How.XPath, Using = "//div[6]/div[2]/div/div/label")]
        private IWebElement ActiveStatusPosition { get; set; }
        // Find  Save Button
        [FindsBy(How = How.Id, Using = "btnSave")]
        private IWebElement SaveButton { get; set; }
        // Find  Company Certification
        [FindsBy(How = How.LinkText, Using = "Company Certification")]
        private IWebElement CompanyCertificationPosition { get; set; }
        // Find  DocType
        [FindsBy(How = How.Id, Using = "ddlDocType")]
        private IWebElement DocTypePosition { get; set; }
        // Find  Certification No
        [FindsBy(How = How.XPath, Using = "//div[2]/div/div[2]/div/input")]
        private IWebElement CertificationNoPosition { get; set; }
        // Find  Issue Authority
        [FindsBy(How = How.Id, Using = "Text1")]
        private IWebElement IssueAuthorityPosition { get; set; }
        

        // Find  Issue Date
        [FindsBy(How = How.XPath, Using = "//body/div/div/table/tbody/tr/td[5]")]
        private IWebElement IssueDatePosition { get; set; }
        // Find  Next Renewal Date
        [FindsBy(How = How.XPath, Using = "//body/div/div/table/tbody/tr[2]/td[4]")]
        private IWebElement NextRenewalDatePosition { get; set; }
        // Find  Note
        [FindsBy(How = How.Id, Using = "Text3")]
        private IWebElement NotePosition { get; set; }
        // Find  File 
        [FindsBy(How = How.Id, Using = "jUploadFile1438166151644")]
        private IWebElement FilePosition { get; set; }
        // Find  Save Certification Button 
        [FindsBy(How = How.Id, Using = "Button2")]
        private IWebElement SaveCertificationPosition { get; set; }
        // Find  Success message
        [FindsBy(How = How.XPath, Using = "//body/div/div/div[2]")]
        private IWebElement SuccessMessagePosition { get; set; }


        //TC/company/001    Verify that "Code" is auto generated from database after saving information
        //Author : TareQ
        public void AllInputFieldsforCompanySetup(string GroupOfCompany,string NameEnglish, string NameNative, string AddressEnglish, string AddressNative, string PhoneNo, string Mobile, string Fax, string Email, string Web,string Logo, string Remarks,string DocType,string CertificationNo,string
IssueAuthority, string IssueDate, string NextRenewalDate, string Notes,string File)
        {
            //Enter Group of Company
            TestActions.selectByVisibleText (GroupOfCompanyPosition, GroupOfCompany);
            //Enter Name English
            TestActions.typeText(NameEnglishPosition, NameEnglish);
            //Enter Name Native
            TestActions.typeText(NameNativePosition, NameNative);
            //Enter Address English
            TestActions.typeText(AddressEnglishPosition, AddressEnglish);
            //Enter Address Native
            TestActions.typeText(AddressNativePosition, AddressNative);
            //Enter Phone No
            TestActions.typeText(PhoneNoPosition, PhoneNo);
            //Enter Mobile
            TestActions.typeText(MobilePosition, Mobile);
            //Enter Fax
            TestActions.typeText(FaxPosition, Fax);
            //Enter Email
            TestActions.typeText(EmailPosition, Email);
            //Enter Web
            TestActions.typeText(WebPosition, Web);
            // logo upload
            LogoPosition.SendKeys(Logo);
            //Enter Remarks
            TestActions.typeText(RemarksPosition, Remarks);
            //click Company Certification
            //CompanyCertificationPosition.Click();
            //Enter Tin
            //TestActions.selectByVisibleText(DocTypePosition, DocType);
            //Enter Certification No.
            //TestActions.typeText(CertificationNoPosition, CertificationNo);
            //Enter Issue Authority.
            //TestActions.typeText(IssueAuthorityPosition, IssueAuthority);
           
            //Enter Issue Date
            //TestActions.selectByVisibleText(IssueDatePosition, IssueDate);
            //Enter Next Renewal Date
            //TestActions.selectByVisibleText(NextRenewalDatePosition, NextRenewalDate);
            //Enter Certification No.
            //TestActions.typeText(NotePosition, Notes);
            // File upload
            //FilePosition.SendKeys(File);
            //save Certification button click
            //SaveCertificationPosition.Click();
            //save button click
            SaveButton.Click();

        }
        public string CheckSuccessMessage()
        {
            string SuccessMessage = SuccessMessagePosition.Text.ToString();
            return SuccessMessage;
        }
    }
}
