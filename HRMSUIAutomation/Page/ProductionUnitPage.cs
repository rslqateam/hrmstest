﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using OpenQA.Selenium;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.PageObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HRMSUIAutomation.Utilities;

namespace HRMSUIAutomation.Page
{
    [TestClass]
    public class ProductionUnitPage
    {
          private IWebDriver driver;

          public ProductionUnitPage(IWebDriver driver)
        {
		    this.driver = driver;
	    }
          // Find  Company
          [FindsBy(How = How.Id, Using = "ddlCompany")]
          private IWebElement CompanyPosition { get; set; }
          // Find  Business Unit
          [FindsBy(How = How.Id, Using = "ddlBusiUnit")]
          private IWebElement BusinessUnitPosition { get; set; }
          // Find  Department
          [FindsBy(How = How.Id, Using = "ddlDepartment")]
          private IWebElement DepartmentPosition { get; set; }
          // Find  Section
          [FindsBy(How = How.Id, Using = "ddlSection")]
          private IWebElement SectionPosition { get; set; }
          // Find  Name(English)
          [FindsBy(How = How.Id, Using = "txtNameEng")]
          private IWebElement NameEnglishPosition { get; set; }
          // Find  Name(Native)
          [FindsBy(How = How.Id, Using = "txtNameBng")]
          private IWebElement NameNativePosition { get; set; }
          // Find  Remarks
          [FindsBy(How = How.Name, Using = "txtRemarks")]
          private IWebElement RemarksPosition { get; set; }
          // Find  Save Button
          [FindsBy(How = How.Id, Using = "Button1")]
          private IWebElement SaveButton { get; set; }
          // Find  Success message
          [FindsBy(How = How.XPath, Using = "//body/div/div/div[2]")]
          private IWebElement SuccessMessagePosition { get; set; }

          //TC/ProductionUnit/001    Verify that "Code" is auto generated from database after saving information
          //Author : TareQ

          public void AllInputFieldsforProductionUnit(string Company, string BusinessUnit, string Department, string Section, string NameEnglish, string NameNative, string Remarks)
          {
              //Enter Company
              TestActions.selectByVisibleText(CompanyPosition, Company);
              //Enter BusinessUnit
              TestActions.selectByVisibleText(BusinessUnitPosition, BusinessUnit);
              //Enter Department
              TestActions.selectByVisibleText(DepartmentPosition, Department);
              //Enter Section
              TestActions.selectByVisibleText(SectionPosition, Section);
              //Enter Name English
              TestActions.typeText(NameEnglishPosition, NameEnglish);
              //Enter Name Native
              TestActions.typeText(NameNativePosition, NameNative);
              //Enter Remarks
              TestActions.typeText(RemarksPosition, Remarks);
              //save button click
              SaveButton.Click();
          }
          public string CheckSuccessMessage()
          {
              string SuccessMessage = SuccessMessagePosition.Text.ToString();
              return SuccessMessage;
          }
    }
}
