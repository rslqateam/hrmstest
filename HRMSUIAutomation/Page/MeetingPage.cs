﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using OpenQA.Selenium;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.PageObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HRMSUIAutomation.Utilities;

namespace HRMSUIAutomation.Page
{
    [TestClass]
    public class MeetingPage
    {
        private IWebDriver driver;

        public MeetingPage(IWebDriver driver)
        {
		    this.driver = driver;
	    }
        // Find  Code
        [FindsBy(How = How.Id, Using = "txtCode")]
        private IWebElement CodePosition { get; set; }
        // Find Meeting Title
        [FindsBy(How = How.Id, Using = "txtMeetingTitle")]
        private IWebElement MeetingTitlePosition { get; set; }
        // Find  Meeting Location
        [FindsBy(How = How.Id, Using = "ddlMeetingLocation")]
        private IWebElement MeetingLocationPosition { get; set; }
        // Find  Meeting From Date
        [FindsBy(How = How.XPath, Using = "//div[2]/div/div/input")]
        private IWebElement MeetingFromDatePosition { get; set; }
        // Find  Meeting To Date
        [FindsBy(How = How.XPath, Using = "//div[2]/div[2]/div/input")]
        private IWebElement MeetingToDatePosition { get; set; }
        // Find  From Time
        [FindsBy(How = How.Id, Using = "Text2")]
        private IWebElement FromTimePosition { get; set; }
        // Find  To Time
        [FindsBy(How = How.Id, Using = "Text3")]
        private IWebElement ToTimePosition { get; set; }
        // Find  Meeting Agenda
        [FindsBy(How = How.Id, Using = "Text4")]
        private IWebElement MeetingAgendaPosition { get; set; }
        // Find  Meeting Details
        [FindsBy(How = How.Id, Using = "Text5")]
        private IWebElement MeetingDetailsPosition { get; set; }
        // Find  Note
        [FindsBy(How = How.Id, Using = "Text6")]
        private IWebElement NotePosition { get; set; }
        //Create Meeting Participants
        // Find  EmployeeId CheckBox
        [FindsBy(How = How.Id, Using = "chkEmpId")]
        private IWebElement EmployeeIdCheckBoxPosition { get; set; }
        // Find  EmployeeId
        [FindsBy(How = How.Id, Using = "txtEmpId")]
        private IWebElement EmployeeIdPosition { get; set; }
        // Find  EmployeeName
        [FindsBy(How = How.Id, Using = "txtEmpName")]
        private IWebElement ParticipantNamePosition { get; set; }
        // Find Participant Designation
        [FindsBy(How = How.Id, Using = "txtDesignation")]
        private IWebElement ParticipantDesignationPosition { get; set; }
        // Find Participant Organisation
        [FindsBy(How = How.Id, Using = "txtOrganisation")]
        private IWebElement ParticipantOrganizationPosition { get; set; }
        // Find  Participant Email
        [FindsBy(How = How.Id, Using = "txtEmail")]
        private IWebElement ParticipantEmailPosition { get; set; }
        // Find Participant Mobile No.
        [FindsBy(How = How.Id, Using = "Text12")]
        private IWebElement ParticipantMobilePosition { get; set; }
        // Find  Participant Address
        [FindsBy(How = How.Id, Using = "Text13")]
        private IWebElement ParticipantAddressPosition { get; set; }
        // Find  Add Button
        [FindsBy(How = How.Id, Using = "Button1")]
        private IWebElement AddButton { get; set; }
        // Find  Save Button
        [FindsBy(How = How.Id, Using = "btnSave")]
        private IWebElement SaveButton { get; set; }
        // Find  Success message
        [FindsBy(How = How.XPath, Using = "//body/div/div/div[2]")]
        private IWebElement SuccessMessagePosition { get; set; }

        //TC/Meeting/001    Verify that "Code" is auto generated from database after saving information
        //Author : TareQ

        public void AllInputFieldsforMeeting( string MeetingTitle, string MeetingLocation, string MeetingFromDate, string MeetingToDate, string FromTime,string ToTime ,string MeetingAgenda,string MeetingDetails,string Note,string EmployeeId,string ParticipantName,string ParticipantDesignation,string ParticipantOrganization,string ParticipantEmail,string ParticipantMobile,string ParticipantAddress)
        {
            // Enter Code
            // TestActions.returnListValueByIndex(GradePosition, IndexOfGrade);
            // Enter MeetingTitle
            TestActions.typeText(MeetingTitlePosition, MeetingTitle);
            //Enter MeetingLocation 
            TestActions.selectByVisibleText(MeetingLocationPosition, MeetingLocation);
            //Enter MeetingFromDate 
            TestActions.typeText(MeetingFromDatePosition, MeetingFromDate);
            //Enter MeetingToDate
            TestActions.typeText(MeetingToDatePosition, MeetingToDate);
            //Enter FromTime
            TestActions.typeText(FromTimePosition, FromTime);
            //Enter MeetingToDate
            TestActions.typeText(ToTimePosition, ToTime);
            //Enter MeetingAgenda
            TestActions.typeText(MeetingAgendaPosition, MeetingAgenda);
            //Enter MeetingDetails
            TestActions.typeText(MeetingDetailsPosition, MeetingDetails);
            //Enter Note
            TestActions.typeText(NotePosition, Note);
            //click Employee Id check button
            EmployeeIdCheckBoxPosition.Click();
            //Enter EmployeeId 
            TestActions.typeText(EmployeeIdPosition, EmployeeId);
            //Enter ParticipantName
            TestActions.typeText(ParticipantNamePosition, ParticipantName);
            //Enter ParticipantDesignation
            TestActions.typeText(ParticipantDesignationPosition, ParticipantDesignation);
            //Enter ParticipantOrganization
            TestActions.typeText(ParticipantOrganizationPosition, ParticipantOrganization);
            //Enter ParticipantEmail
            TestActions.typeText(ParticipantEmailPosition, ParticipantEmail);
            //Enter ParticipantMobile
            TestActions.typeText(ParticipantMobilePosition, ParticipantMobile);
            //Enter ParticipantAddress
            TestActions.typeText(ParticipantAddressPosition, ParticipantAddress);
            //save button click
            AddButton.Click();
            //save button click
            SaveButton.Click();
        }
        public string CheckSuccessMessage()
        {
            string SuccessMessage = SuccessMessagePosition.Text.ToString();
            return SuccessMessage;
        }
    }
}
